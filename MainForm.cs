﻿using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using IdsImport;
using IdsQuickBooks.Properties;
using IppDotNetSdkQuickBooks;

namespace IdsQuickBooks
{
	public partial class MainForm : Form
	{
		private bool AllowClose = true,
			Abort;

		private string PrevAccessKey = "",
			PrevAccessSecret = "";

		private Import ImportClass;

		public MainForm()
		{
			InitializeComponent();
		}

		private void LoadSettings()
		{
			var D = Settings.Default;
			D.Reload();

			QuickbooksCompanyFile.Text = D.CompanyFile;

			Debug.Checked = D.Debug;
			UserName.Text = D.UserName;
			Password.Text = D.Password;
			IDSExportPath.Text = D.IdsExportPath;
			ErrorFolder.Text = D.ErrorFolder;
			InventoryCodeStructure.Text = D.InventoryCodeStructure;
			CompanyId.Text = D.CompanyId;
			DiscountItemCode.Text = D.DiscountItemCode;
			FuelSurchargeCode.Text = D.FuelSurchargeCode;
			DefaultInvoiceLayout.Text = D.DefaultInvoiceLayout;
			IncludeCharges.Checked = D.IncludeCharges;

			SalesLedger.Text = D.SalesLedger;
			AssetLedger.Text = D.AssetLedger;
			CostOfGoodsLedger.Text = D.CostOfGoodsLedger;
			FuelSurchargeLedger.Text = D.FuelSurchargeLedger;

			AccessTokenKey.Text = D.AccessToken;
			AccessTokenSecret.Text = D.AccessSecret;

			OnlineRadioButton.Checked = D.Online;
			DesktopRadioButton.Checked = D.Desktop;

			autoHideOnStartupToolStripMenuItem.CheckState = D.AutoStart ? CheckState.Checked : CheckState.Unchecked;

			if( D.AutoStart )
			{
				HideButton_Click( this, null );
				TrayIcon.BalloonTipText = @"Minimised";
				TrayIcon.ShowBalloonTip( 10000 );
			}

			if( D.Online )
				OnlineRadioButton_Click( null, null );
			else
				DesktopRadioButton_Click( null, null );
		}


		private void SaveSettings()
		{
			var D = Settings.Default;

			D.CompanyFile = QuickbooksCompanyFile.Text;

			D.Debug = Debug.Checked;
			D.UserName = UserName.Text;
			D.Password = Password.Text;
			D.IdsExportPath = IDSExportPath.Text;
			D.ErrorFolder = ErrorFolder.Text;
			D.InventoryCodeStructure = InventoryCodeStructure.Text;
			D.CompanyId = CompanyId.Text;
			D.DiscountItemCode = DiscountItemCode.Text;
			D.FuelSurchargeCode = FuelSurchargeCode.Text;
			D.DefaultInvoiceLayout = DefaultInvoiceLayout.Text;
			D.IncludeCharges = IncludeCharges.Checked;

			D.SalesLedger = SalesLedger.Text;
			D.AssetLedger = AssetLedger.Text;
			D.CostOfGoodsLedger = CostOfGoodsLedger.Text;
			D.FuelSurchargeLedger = FuelSurchargeLedger.Text;

			D.AccessToken = AccessTokenKey.Text;
			D.AccessSecret = AccessTokenSecret.Text;

			D.AutoStart = autoHideOnStartupToolStripMenuItem.CheckState == CheckState.Checked;

			D.Desktop = DesktopRadioButton.Checked;
			D.Online = OnlineRadioButton.Checked;

			D.Save();
		}

		private Import.INVENTORY_CODE_STRUCTURE InventoryTypeToInventoryCodeType()
		{
			switch( InventoryCodeStructure.Text.Trim() )
			{
			case "Package Type - Service Type":
				return Import.INVENTORY_CODE_STRUCTURE.BY_PACKAGE_SERVICE;
			default:
				return Import.INVENTORY_CODE_STRUCTURE.BY_SERVICE_PACKAGE;
			}
		}


		private void MainForm_Resize( object sender, EventArgs e )
		{
			switch( WindowState )
			{
			case FormWindowState.Minimized:
				TrayIcon.Visible = true;
				ShowInTaskbar = false;
				PollTimer.Enabled = true;
				break;

			default:
				TrayIcon.Visible = false;
				ShowInTaskbar = true;
				PollTimer.Enabled = false;
				break;
			}
		}

		private void TrayIcon_Click( object sender, EventArgs e )
		{
			WindowState = FormWindowState.Normal;
		}

		private void MainForm_FormClosing( object sender, FormClosingEventArgs e )
		{
			SaveSettings();

			if( AllowClose )
			{
				if( !Abort )
				{
					if( MessageBox.Show( @"Minimise to the icon tray?", @"Hide", MessageBoxButtons.YesNo, MessageBoxIcon.Question )
						== DialogResult.Yes )
					{
						e.Cancel = true;
						WindowState = FormWindowState.Minimized;
					}
				}
			}
			else
			{
				MessageBox.Show( @"Currently importing data. Cannot close at this time", @"Currently Importing",
					MessageBoxButtons.OK, MessageBoxIcon.Error );
				e.Cancel = true;
			}
		}

		private void MainForm_Load( object sender, EventArgs e )
		{
			LoadSettings();
			OptionsTabControl.Appearance = TabAppearance.FlatButtons;
			OptionsTabControl.ItemSize = new Size( 0, 1 );
			OptionsTabControl.SizeMode = TabSizeMode.Fixed;
		}

		private void MainForm_FormClosed( object sender, FormClosedEventArgs e )
		{
			SaveSettings();
			TrayIcon.Visible = false;
		}

		private void button3_Click( object sender, EventArgs e )
		{
			FolderBrowserDialog.SelectedPath = IDSExportPath.Text;
			if( FolderBrowserDialog.ShowDialog() == DialogResult.OK )
				IDSExportPath.Text = FolderBrowserDialog.SelectedPath;
		}

		private void button1_Click( object sender, EventArgs e )
		{
			FolderBrowserDialog.SelectedPath = ErrorFolder.Text;
			if( FolderBrowserDialog.ShowDialog() == DialogResult.OK )
				ErrorFolder.Text = FolderBrowserDialog.SelectedPath;
		}


		private bool DontSetBalloonTip;

		private void TrayIcon_BalloonTipClosed( object sender, EventArgs e )
		{
			if( !DontSetBalloonTip )
				TrayIcon.BalloonTipText = @"Import Idle";
		}


		private bool InImport;

		private void PollTimer_Tick( object sender, EventArgs e )
		{
			if( !InImport )
			{
				InImport = true;
				PollTimer.Enabled = false;
				try
				{
					var Files = Directory.GetFiles( IDSExportPath.Text, @"*.csv" );
					Array.Sort( Files );

					if( Files.Length > 0 )
					{
						AllowClose = false;
						exitToolStripMenuItem.Enabled = false;
						fileToolStripMenuItem2.Enabled = false;

						DontSetBalloonTip = true;
						TrayIcon.BalloonTipText = @"Importing Data";
						TrayIcon.ShowBalloonTip( 10000 );

						var CodeType = InventoryTypeToInventoryCodeType();
						var Pw = Password.Text;
						var CoId = CompanyId.Text.Trim();
						var ErrF = ErrorFolder.Text.Trim();
						var Dbug = Debug.Checked;
						const string Country = "CA";

						ImportClass = OnlineRadioButton.Checked
							? new QuickBooks.Online.Import()
							: (Import) new QuickBooks.Desktop.Import( QuickbooksCompanyFile.Text, Country,
								AssetLedger.Text, SalesLedger.Text, CostOfGoodsLedger.Text, FuelSurchargeLedger.Text,
								DiscountItemCode.Text, FuelSurchargeCode.Text, DefaultInvoiceLayout.Text, IncludeCharges.Checked );

						try
						{
							foreach( var File in Files )
							{
								var Importing = true;

								var File2 = File;
								Task.Factory.StartNew( () =>
								{
									var T = new Task[ 1 ];

									var File1 = File2;
									T[ 0 ] = Task.Factory.StartNew( () => { ImportClass.Execute( CoId, Pw, CodeType, File1, ErrF, Dbug ); } );

									Task.WaitAll( T );
									Importing = false;
								} );

								while( Importing )
									Application.DoEvents();
							}
						}
						finally
						{
							AllowClose = true;
							exitToolStripMenuItem.Enabled = true;
							fileToolStripMenuItem2.Enabled = true;

							DontSetBalloonTip = false;
							TrayIcon.BalloonTipText = @"Import Complete";
							TrayIcon.ShowBalloonTip( 10000 );
						}
					}
				}
				catch
				{
				}
				finally
				{
					InImport = false;
					AllowClose = true;

					if( sender != null ) // Import Btn
						PollTimer.Enabled = true;
				}
			}
		}

		private void HideButton_Click( object sender, EventArgs e )
		{
			SaveSettings();
			WindowState = FormWindowState.Minimized;
		}

		private void exitToolStripMenuItem1_Click( object sender, EventArgs e )
		{
			Close();
		}

		private void showToolStripMenuItem_Click( object sender, EventArgs e )
		{
			WindowState = FormWindowState.Normal;
		}

		private void AccessTokenKey_Enter( object sender, EventArgs e )
		{
			PrevAccessKey = AccessTokenKey.Text.Trim();
		}

		private void AccessTokenSecret_Enter( object sender, EventArgs e )
		{
			PrevAccessSecret = AccessTokenSecret.Text.Trim();
		}

		private void AccessTokenKey_Leave( object sender, EventArgs e )
		{
			var Temp = AccessTokenKey.Text.Trim();
			AccessTokenKey.Text = Temp;

			if( Temp != PrevAccessKey )
				Constants.AccessToken.Key = Temp;
		}

		private void AccessTokenSecret_Leave( object sender, EventArgs e )
		{
			var Temp = AccessTokenSecret.Text.Trim();
			AccessTokenSecret.Text = Temp;

			if( Temp != PrevAccessSecret )
				Constants.AccessToken.Secret = Temp;
		}

		private void showToolStripMenuItem1_Click( object sender, EventArgs e )
		{
			WindowState = FormWindowState.Normal;
		}

		private void ImportBtn_Click( object sender, EventArgs e )
		{
			ImportBtn.Enabled = false;
			PollTimer_Tick( null, null );
			MessageBox.Show( @"Import Completed", @"Completed", MessageBoxButtons.OK );
			ImportBtn.Enabled = true;
		}

		private void exitToolStripMenuItem_Click_1( object sender, EventArgs e )
		{
			Abort = true;
			Close();
		}

		private void CompanyId_Leave( object sender, EventArgs e )
		{
			CompanyId.Text = CompanyId.Text.Trim();
		}

		private void OnlineRadioButton_Click( object sender, EventArgs e )
		{
			OptionsTabControl.SelectedIndex = 0;
		}

		private void DesktopRadioButton_Click( object sender, EventArgs e )
		{
			OptionsTabControl.SelectedIndex = 1;
		}

		private void button2_Click( object sender, EventArgs e )
		{
			OpenFileDialog.FileName = QuickbooksCompanyFile.Text;
			if( OpenFileDialog.ShowDialog() == DialogResult.OK )
				QuickbooksCompanyFile.Text = OpenFileDialog.FileName.Trim();
		}

		private void QuickbooksCompanyFile_Leave( object sender, EventArgs e )
		{
			QuickbooksCompanyFile.Text = QuickbooksCompanyFile.Text.Trim();
		}

		private void SalesLedger_Leave( object sender, EventArgs e )
		{
			SalesLedger.Text = SalesLedger.Text.Trim();
		}

		private void AssetLedger_Leave( object sender, EventArgs e )
		{
			AssetLedger.Text = AssetLedger.Text.Trim();
		}

		private void CostOfGoodsLedger_Leave( object sender, EventArgs e )
		{
			CostOfGoodsLedger.Text = CostOfGoodsLedger.Text.Trim();
		}

		private void DiscountItemCode_Leave( object sender, EventArgs e )
		{
			DiscountItemCode.Text = DiscountItemCode.Text.Trim();
		}

		private void FuelSurchargeLedger_Leave( object sender, EventArgs e )
		{
			FuelSurchargeLedger.Text = FuelSurchargeLedger.Text.Trim();
		}

		private void FuelSurchargeCode_Leave( object sender, EventArgs e )
		{
			FuelSurchargeCode.Text = FuelSurchargeCode.Text.Trim();
		}

		private void DefaultInvoiceLayout_Leave( object sender, EventArgs e )
		{
			DefaultInvoiceLayout.Text = DefaultInvoiceLayout.Text.Trim();
		}
	}
}