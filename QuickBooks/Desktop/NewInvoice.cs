﻿using System;
using QBFC13Lib;

namespace IdsQuickBooks.QuickBooks.Desktop
{
	public partial class Import
	{
		private string CurrentInvoiceNumber;
		private IInvoiceAdd CurrentInvoice;

//		private readonly string RecoveryGuid = '{' + Guid.NewGuid().ToString() + '}';

		public class PostException : Exception
		{
			public PostException( string invoiceNumber ) : base( "Cannot post invoice : " + invoiceNumber )
			{
			}
		}

		internal IInvoiceAdd NewInvoice( string currentCustomer, DateTime invoiceDate, string invoiceNumber )
		{
/*
			// (1) Set the error recovery ID using ErrorRecoveryID function
			SessionManager.ErrorRecoveryID.SetValue( RecoveryGuid );

			// (2) Set EnableErrorRecovery to true to enable error recovery
			SessionManager.EnableErrorRecovery = true;

			// (3) Set SaveAllMsgSetRequestInfo to true so the entire contents of the MsgSetRequest
			//		will be saved to disk. If SaveAllMsgSetRequestInfo is false (default), only the 
			//		newMessageSetID will be saved. 
			SessionManager.SaveAllMsgSetRequestInfo = true;


			// (4) Use IsErrorRecoveryInfo to check whether an unprocessed response exists. 
			//		If IsErrorRecoveryInfo is true:
			if( SessionManager.IsErrorRecoveryInfo() )
			{
				// a. Get the response status, using GetErrorRecoveryStatus
				var ResMsgSet = SessionManager.GetErrorRecoveryStatus();
				switch( ResMsgSet.Attributes.MessageSetStatusCode )
				{
				case "600":
				case "1901":
				case "1904":
				case "1905":
					throw new PostException( invoiceNumber );

				default:
					var res = ResMsgSet.ResponseList.GetAt( 0 );
					var sCode = res.StatusCode;
					//string sMessage = res.StatusMessage;
					//string sSeverity = res.StatusSeverity;

					if( sCode < 0 ) // 0 == OK , > 0 == Warning
					{
						// b. Get the saved request, using GetSavedMsgSetRequest
						var ReqMsgSet = SessionManager.GetSavedMsgSetRequest();
						//reqXML = reqMsgSet.ToXMLString();
						//MessageBox.Show(reqXML);

						// c. Reprocess the response, possibly using the saved request
						var ReqMsgSet1 = SessionManager.DoRequests( ReqMsgSet );
						var resp = ReqMsgSet1.ResponseList.GetAt( 0 );
						var statCode = resp.StatusCode;
						if( statCode < 0 )
							throw new PostException( "Pervious to " + invoiceNumber );
					}
					SessionManager.ClearErrorRecovery();
					break;
				}
			}
*/
			// Add the request to the message set request object
			var InvoiceAdd = NewInvoiceRequestSet.AppendInvoiceAddRq();
			InvoiceAdd.CustomerRef.FullName.SetValue( currentCustomer );

			InvoiceAdd.TemplateRef.FullName.SetValue( InvoiceFormat );

			InvoiceAdd.TxnDate.SetValue( invoiceDate );
			InvoiceAdd.RefNumber.SetValue( invoiceNumber );

			CurrentInvoiceNumber = invoiceNumber;

			return CurrentInvoice = InvoiceAdd;
		}

		internal void EndInvoice()
		{
			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if( TotalFuelCharge != 0 )
			{
				BlankLine();
				NewInvoiceLine( ChargeFuelKey, ChargeFuelDesc, (decimal) TotalFuelCharge );
				TotalFuelCharge = 0;
			}

			var ResponseMsgSet = DoInvoicRequests();
			var response = ResponseMsgSet.ResponseList.GetAt( 0 );
			if( response.StatusCode != 0 )
				throw new PostException( "Cannot post invoice" + CurrentInvoiceNumber );
		}

		internal void NameAndAddress( string companyName, string address1, string address2,
									  string address3, string address4,
									  string city, string region, string postCode,
									  string country, string contact, string phone, string email )
		{
			var BillAddress = CurrentInvoice.BillAddress;

			BillAddress.Addr1.SetValue( address1 );
			BillAddress.Addr2.SetValue( address2 );
			BillAddress.Addr3.SetValue( address3 );
			BillAddress.Addr4.SetValue( address4 );
			BillAddress.City.SetValue( city );
			BillAddress.State.SetValue( region );
			BillAddress.PostalCode.SetValue( postCode );
			BillAddress.Country.SetValue( country );
		}
	}
}