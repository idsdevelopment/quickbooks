﻿using System;
using IdsImport;
using QBFC13Lib;

namespace IdsQuickBooks.QuickBooks.Desktop
{
	public partial class Import : IdsImport.Import
	{
		private const string CHARGE_CODE = "Charge",
							 CHARGE_DESC = "Charges";

		private readonly string Country,
								FileName,
								AssetLedger,
								SalesLedger,
								CostOfGoodsLedger,
								FuelSurchargeLedger,
								DiscountItemCode,
								FuelSurchargeCode,
								InvoiceFormat;

		private readonly bool AddChargesIntoTrip;

		protected override void OnDispose( bool systemDisposing )
		{
			EndSession();
		}

		public Import( string qbFileName, string country, string assetLedger, string salesLedger, string costOfGoodsLedger,
					   string fuelSurchargeLedger,
					   string discountItemCode, string fuelSurchargeCode, string invoiceFormat, bool addChargesIntoTrip )
		{
			AssetLedger = assetLedger;
			SalesLedger = salesLedger;
			CostOfGoodsLedger = costOfGoodsLedger;
			FuelSurchargeLedger = fuelSurchargeLedger;
			InvoiceFormat = invoiceFormat;

			AddChargesIntoTrip = addChargesIntoTrip;

			DiscountItemCode = discountItemCode;
			FuelSurchargeCode = fuelSurchargeCode;

			FileName = qbFileName;
			Country = country;
			SessionManager.OpenConnection( "", "IDS Import" );
		}

		protected override void OnException( Row csvRow, Exception exception )
		{
			if( RequestSet != null )
				RequestSet.ClearRequests(); // Throw away updates
			if( InvoiceRequestSet != null )
				InvoiceRequestSet.ClearRequests();
		}

		protected override void BeginUpdate( string errorPath, bool debug )
		{
			SessionManager.BeginSession( FileName, ENOpenMode.omDontCare );
			InBeginSession = true;
		}

		protected override void EndUpdate( bool errors )
		{
			InBeginSession = true;
			SessionManager.EndSession();
		}

		private string TaxCode;
		private bool NeedBlankLine;

		private double TotalCharges, TotalFuelCharge;

		protected override void BeginInvoice( string accountNumber, string invoiceNumber, DateTime invoiceDate, string taxCode,
											  bool exempt,
											  Row accountLineRow )
		{
			NeedBlankLine = false;
			TotalCharges = TotalFuelCharge = 0;

			TaxCode = exempt ? "EXEMPT" : taxCode;

			string CompanyName = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_COMPANY ];
			string Address1 = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_ADDRESS_1 ];
			string Address2 = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_ADDRESS_2 ];

			var Address3 = "";
			var Address4 = "";

			string City = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_CITY ];
			string Region = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_REGION ];
			string PostCode = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_POST_CODE ];
			string CountryCode = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_COUNTRY ];
			string Contact = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_CONTACT ];
			string Phone = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_TELEPHONE ];
			string Email = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_EMAIL ];

			var Cust = AddUpdateCustomer( accountNumber, taxCode, CompanyName, Address1, Address2, City, Region, PostCode,
										  CountryCode, Contact, Phone, Email );

			if( Cust != null )
			{
				if( Cust.CompanyName != null )
					CompanyName = Cust.CompanyName.GetValue();

				var BillTo = Cust.BillAddress;
				if( BillTo != null )
				{
					Address1 = BillTo.Addr1 != null ? BillTo.Addr1.GetValue() : "";
					Address2 = BillTo.Addr2 != null ? BillTo.Addr2.GetValue() : "";
					Address3 = BillTo.Addr3 != null ? BillTo.Addr3.GetValue() : "";
					Address4 = BillTo.Addr4 != null ? BillTo.Addr4.GetValue() : "";

					City = BillTo.City != null ? BillTo.City.GetValue() : "";
					Region = BillTo.State != null ? BillTo.State.GetValue() : "";
					PostCode = BillTo.PostalCode != null ? BillTo.PostalCode.GetValue() : "";
					CountryCode = BillTo.Country != null ? BillTo.Country.GetValue() : "";
				}

				var Inv = NewInvoice( accountNumber, invoiceDate, invoiceNumber );

				var Method = Cust.PreferredDeliveryMethod;

				if( Method != null )
				{
					try			// Catch bug in Com Object HRESULT Error
					{
						switch( Method.GetValue() )
						{
						case ENPreferredDeliveryMethod.pdmEmail:
							var E = Cust.Email;
							if( E != null && Utils.Email.IsValidAddress( E.GetValue() ) )
								Inv.IsToBeEmailed.SetValue( true );
							break;
						case ENPreferredDeliveryMethod.pdmNone:
						case ENPreferredDeliveryMethod.pdmFax:
							break;
						default:
							Inv.IsToBePrinted.SetValue( true );
							break;
						}
					}
					catch
					{
						Inv.IsToBePrinted.SetValue( true );
					}
				}
				NameAndAddress( CompanyName, Address1, Address2, Address3, Address4, City, Region, PostCode, CountryCode, Contact, Phone, Email );
			}
			else
				throw new Exception( "Lost Customer: " + accountNumber );
		}

		protected override void EndOfInvoiceLine( Row csvRow, bool errorThrown )
		{
			DoCharges();
		}

		protected override void EndInvoice( bool errorThrown )
		{
			if( !errorThrown )
				EndInvoice();
			else
			{
				if( RequestSet != null )
					RequestSet.ClearRequests(); // Throw away updates
				if( InvoiceRequestSet != null )
					InvoiceRequestSet.ClearRequests();
			}
		}


		private IInvoiceLineAdd CurrentInvoiceLine;


		private static double Round2( double val )
		{
			return Math.Round( val, 2, MidpointRounding.AwayFromZero );
		}

		private static double Round2( decimal val )
		{
			return Round2( (double) val );
		}

		internal IInvoiceLineAdd NewInvoiceLine( string itemName, string itemDescription, decimal amount, decimal quantity = 1 )
		{
			var InvoiceLine = CurrentInvoice.ORInvoiceLineAddList.Append().InvoiceLineAdd;
			InvoiceLine.ItemRef.FullName.SetValue( itemName );
			SetValue( InvoiceLine.Desc, itemDescription );
			InvoiceLine.ORRatePriceLevel.Rate.SetValue( Round2( amount ) );
			InvoiceLine.Quantity.SetValue( Round2( quantity ) );
			InvoiceLine.Amount.SetValue( Round2( amount * quantity ) );
			return CurrentInvoiceLine = InvoiceLine;
		}

		internal void NewDiscountLine( decimal amount )
		{
			var Desc = "Item: " + DiscountItemCode;
			AddUpdateInventory( DiscountItemCode, Desc, TaxCode, SalesLedger );
			NewInvoiceLine( DiscountItemCode, Desc, -amount, -1 );
		}

		internal void NewCommentLine( string comment )
		{
			CurrentInvoice.ORInvoiceLineAddList.Append().InvoiceLineAdd.Desc.SetValue( comment );
		}

		private string PickupAddress, DeliveryAddress, Reference, Pod;
		private DateTime ServiceDate;

		protected override void AccountLine( Row csvRow )
		{
			if( !DateTime.TryParse( csvRow[ (int) IdsImport.AccountLine.FIELDS.CALL_TIME ].AsString, out ServiceDate ) )
				ServiceDate = DateTime.Now;

			PickupAddress = csvRow[ (int) IdsImport.AccountLine.FIELDS.PICKUP_COMPANY ].AsString.Trim()
							+ ' ' + csvRow[ (int) IdsImport.AccountLine.FIELDS.PICKUP_ADDRESS_1 ].AsString.Trim()
							+ ' ' + csvRow[ (int) IdsImport.AccountLine.FIELDS.PICKUP_ADDRESS_2 ].AsString.Trim()
							+ ' ' + csvRow[ (int) IdsImport.AccountLine.FIELDS.PICKUP_CITY ].AsString.Trim();

			DeliveryAddress = csvRow[ (int) IdsImport.AccountLine.FIELDS.DELIVERY_COMPANY ].AsString.Trim()
							  + ' ' + csvRow[ (int) IdsImport.AccountLine.FIELDS.DELIVERY_ADDRESS_1 ].AsString.Trim()
							  + ' ' + csvRow[ (int) IdsImport.AccountLine.FIELDS.DELIVERY_ADDRESS_2 ].AsString.Trim()
							  + ' ' + csvRow[ (int) IdsImport.AccountLine.FIELDS.DELIVERY_CITY ].AsString.Trim();

			Reference = csvRow[ (int) IdsImport.AccountLine.FIELDS.REFERENCE ].AsString.Trim();

			var P = csvRow[ (int) IdsImport.AccountLine.FIELDS.POD ].AsString.Trim();
			Pod = P != "" ? "POD: " + P : "";
		}


		private void DoCharges()
		{
			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if( TotalCharges != 0 )
			{
				NewInvoiceLine( CHARGE_CODE, CHARGE_DESC, (decimal) TotalCharges );
				TotalCharges = 0;
			}
		}

		protected void BlankLine()
		{
			CurrentInvoiceLine = CurrentInvoice.ORInvoiceLineAddList.Append().InvoiceLineAdd;
			SetValue( CurrentInvoiceLine.Desc, "" );
		}

		private bool ZeroAmount;
		private IInvoiceLineAdd CurrentTripLine;

		protected override void TripLine( Row csvRow )
		{
			CurrentTripLine = null;

			DoCharges();


			if( NeedBlankLine )
				BlankLine();
			else
				NeedBlankLine = true;


			CurrentInvoiceLine = CurrentInvoice.ORInvoiceLineAddList.Append().InvoiceLineAdd;
			CurrentInvoiceLine.ServiceDate.SetValue( ServiceDate );
			SetValue( CurrentInvoiceLine.Desc, PickupAddress );

			var TripId = "Trip ID: " + csvRow[ (int) IdsImport.TripLine.FIELDS.TRIP_ID ];
			SetValue( CurrentInvoiceLine.Other1, TripId );

			var Weight = "Wgt: " + csvRow[ (int) IdsImport.TripLine.FIELDS.WEIGHT ].AsString.Trim();
			SetValue( CurrentInvoiceLine.Other2, Weight );

			CurrentInvoiceLine = CurrentInvoice.ORInvoiceLineAddList.Append().InvoiceLineAdd;
			SetValue( CurrentInvoiceLine.Desc, DeliveryAddress );
			SetValue( CurrentInvoiceLine.Other1, "REF: " + Reference );
			var Pieces = "Pieces: " + csvRow[ (int) IdsImport.TripLine.FIELDS.PIECES ].AsString.Trim();
			SetValue( CurrentInvoiceLine.Other2, Pieces );


			var InventoryKey = TripInventoryKey( csvRow );
			AddUpdateInventory( InventoryKey, "Item: " + InventoryKey, TaxCode, SalesLedger );

			var Item = InventoryCodes[ InventoryKey ];
			var Amount = (decimal) csvRow[ (int) IdsImport.TripLine.FIELDS.BASE_RATE ].AsDouble;

			Amount = Math.Round( Amount, 4, MidpointRounding.AwayFromZero );

			ZeroAmount = Amount == 0; // Show Charges
			if( ZeroAmount )
				NewCommentLine( InventoryKey );
			else
			{
				CurrentTripLine = NewInvoiceLine( Item.Code, Item.Description, Amount );
				if( Pod != "" )
					SetValue( CurrentTripLine.Other1, Pod );
			}

			var Discount = -(decimal) csvRow[ (int) IdsImport.TripLine.FIELDS.DISCOUNT_AMOUNT ].AsDouble;
			if( Discount != 0 )
				NewDiscountLine( Discount );
		}

		private string ChargeFuelKey = "", ChargeFuelDesc = "";

		protected override void ChargeLine( Row csvRow, double amount )
		{
			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if( amount != 0 )
			{
				amount = Math.Round( amount, 4, MidpointRounding.AwayFromZero );

				var Charge = csvRow[ (int) IdsImport.ChargeLine.FIELDS.DESCRIPTION ].AsString.Trim();

				if( Charge == FuelSurchargeCode )
				{
					var InventoryKey = ChargeInventoryKey( csvRow );
					ChargeFuelKey = InventoryKey;

					var Code = "Charge: " + Charge;
					ChargeFuelDesc = Code;

					AddUpdateInventory( InventoryKey, Code, TaxCode, FuelSurchargeLedger );
					TotalFuelCharge += amount;
				}
				else if( ZeroAmount || !AddChargesIntoTrip || CurrentTripLine == null )
				{
					AddUpdateInventory( CHARGE_CODE, CHARGE_DESC, TaxCode, SalesLedger );
					TotalCharges += amount;
				}
				else
				{
					var Amount = CurrentTripLine.Amount;
					Amount.SetValue( Round2( Amount.GetValue() + amount ) );
				}
			}
		}

		protected override void TaxLine( Row csvRow, double amount )
		{
		}
	}
}