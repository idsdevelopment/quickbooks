﻿using System;
using QBFC13Lib;

namespace IdsQuickBooks.QuickBooks.Desktop
{
	public partial class Import
	{
		private readonly QBSessionManager SessionManager = new QBSessionManager();
		private bool InBeginSession;

		private short MajorVersion, MinorVersion;
		private double? _LastVers;

		private void EndSession()
		{
			if( InBeginSession )
			{
				SessionManager.ClearErrorRecovery();
				SessionManager.EndSession();
			}
			SessionManager.CloseConnection();
		}


		// Code for handling different versions of QuickBooks
		private double? GetLatestVersion( string country )
		{
			if( _LastVers == null )
			{
				var MsgSetRequest = SessionManager.CreateMsgSetRequest( country, 13, 0 );
				MsgSetRequest.AppendHostQueryRq();
				var QueryResponse = SessionManager.DoRequests( MsgSetRequest );

				// The response list contains only one response,
				// which corresponds to our single HostQuery request
				var Response = QueryResponse.ResponseList.GetAt( 0 );

				var HostResponse = Response.Detail as IHostRet;
				if( HostResponse != null )
				{
					var SupportedVersions = HostResponse.SupportedQBXMLVersionList;

					double LastVers = 0;

					for( var I = 0; I <= SupportedVersions.Count - 1; I++ )
					{
						var Svers = SupportedVersions.GetAt( I );
						var Vers = Convert.ToDouble( Svers );
						if( Vers > LastVers )
							LastVers = Vers;
					}
					_LastVers = LastVers;
				}
			}
			return _LastVers;
		}

		public IMsgSetRequest GetLatestMsgSet( string country )
		{
			if( _LastVers == null )
			{
				MajorVersion = 1;
				MinorVersion = 0;

				// Find and adapt to supported version of QuickBooks
				var SupportedVersion = GetLatestVersion( country );

				if( SupportedVersion != null )
				{
					if( SupportedVersion >= 13.0 )
					{
						MajorVersion = 13;
						MinorVersion = 0;
					}
					else if( SupportedVersion >= 2.0 )
					{
						MajorVersion = (short) SupportedVersion;
						MinorVersion = 0;
					}
					else if( SupportedVersion >= 1.1 )
					{
						MajorVersion = 1;
						MinorVersion = 1;
					}
				}
			}
			// Create the message set request object
			return SessionManager.CreateMsgSetRequest( country, MajorVersion, MinorVersion );
		}


		private IMsgSetRequest DoNewRequestSet( IMsgSetRequest requestSet )
		{
			if( requestSet != null && requestSet.RequestList.Count > 0 )
				SessionManager.DoRequests( requestSet );

			var Req = GetLatestMsgSet( Country );
			Req.Attributes.OnError = ENRqOnError.roeStop;
			return Req;
		}

		private IMsgSetRequest RequestSet;

		private IMsgSetRequest NewRequestSet
		{
			get { return RequestSet = DoNewRequestSet( RequestSet ); }
		}


		private static void CheckRequestError( IMsgSetResponse setResponse, string errorMessage )
		{
			var response = setResponse.ResponseList.GetAt( 0 );
			if( response.StatusCode != 0 )
			{
				var Message = response.StatusMessage;
				throw new Exception( errorMessage + "\r\nReason:" + Message );
			}
		}

		private IMsgSetResponse DoRequests( string errorMessage )
		{
			if( RequestSet != null )
			{
				var Result = SessionManager.DoRequests( RequestSet );
				RequestSet = null;
				if( errorMessage != null )
					CheckRequestError( Result, errorMessage );
				return Result;
			}
			throw new Exception( "NULL Request Set" );
		}


		private IMsgSetRequest InvoiceRequestSet;

		private IMsgSetRequest NewInvoiceRequestSet
		{
			get { return InvoiceRequestSet = DoNewRequestSet( InvoiceRequestSet ); }
		}


		private IMsgSetResponse DoInvoicRequests()
		{
			if( InvoiceRequestSet != null )
			{
				var Result = SessionManager.DoRequests( InvoiceRequestSet );
				InvoiceRequestSet = null;
				CheckRequestError( Result, "Cannot create invoice." );
				return Result;
			}
			throw new Exception( "NULL Invoice Request Set" );
		}
	}
}