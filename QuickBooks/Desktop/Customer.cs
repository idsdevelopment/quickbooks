﻿using QBFC13Lib;

namespace IdsQuickBooks.QuickBooks.Desktop
{
	public partial class Import
	{
		internal ICustomerRet GetCustomer( string customerCode )
		{
			var CustQ = NewRequestSet.AppendCustomerQueryRq();

			// Optionally, you can put filter on it.
			CustQ.ORCustomerListQuery.FullNameList.Add( customerCode );

			var ResponseSet = DoRequests( null );
			if( ResponseSet != null )
			{
				var Response = ResponseSet.ResponseList.GetAt( 0 );

				if( Response != null )
				{
					var CustomerRetList = Response.Detail as ICustomerRetList;
					if( CustomerRetList != null && CustomerRetList.Count > 0 )
						return CustomerRetList.GetAt( 0 );
				}
			}
			return null;
		}

		private static string ToLength( string text, int length )
		{
			if( text.Length > length )
				text = text.Substring( 0, length );
			return text;
		}

		private static void SetValue( IQBStringType field, string value )
		{
			field.SetValue( ToLength( value, int.Parse( field.GetMaxLength().Trim() ) ) );
		}

		internal ICustomerRet AddUpdateCustomer( string idsAccountNumber, string taxCode, string companyName,
												 string address1, string address2,
												 string city, string region, string postCode,
												 string country, string contact, string phone, string email )
		{
			var Cust = GetCustomer( idsAccountNumber );
			if( Cust == null )
			{
				var Cr = NewRequestSet.AppendCustomerAddRq();

				SetValue( Cr.AccountNumber, idsAccountNumber );
				SetValue( Cr.Name, idsAccountNumber );

				companyName = string.IsNullOrEmpty( companyName ) ? "Unknown" : companyName;

				SetValue( Cr.CompanyName, companyName );

				var BillTo = Cr.BillAddress;

				SetValue( BillTo.Addr1, address1 );
				SetValue( BillTo.Addr2, address2 );
				SetValue( BillTo.City, city );
				SetValue( BillTo.State, region );
				SetValue( BillTo.Country, country );
				SetValue( BillTo.PostalCode, postCode );

				SetValue( Cr.Contact, contact );
				SetValue( Cr.Email, email );
				SetValue( Cr.Phone, phone );

				Cr.PreferredDeliveryMethod.SetValue( ENPreferredDeliveryMethod.pdmEmail );

				DoRequests( "Cannot create customer: " + idsAccountNumber );

				Cust = GetCustomer( idsAccountNumber );
			}
			return Cust;
		}
	}
}