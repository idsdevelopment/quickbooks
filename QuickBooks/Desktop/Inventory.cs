﻿using System;
using System.Collections.Generic;
using QBFC13Lib;

namespace IdsQuickBooks.QuickBooks.Desktop
{
	public partial class Import
	{
		private class Inventory
		{
			internal readonly string Code;
			internal readonly string Description;
			internal readonly string TaxCode;

			internal Inventory( IItemInventoryRet inv )
			{
				if( inv == null )
					throw new PostException( "Lost Inventory Code" );

				Code = inv.Name.GetValue();
				Description = inv.SalesDesc.GetValue();
				TaxCode = inv.SalesTaxCodeRef.FullName.GetValue();
			}
		}

		private readonly Dictionary<string, Inventory> InventoryCodes = new Dictionary<string, Inventory>();


		private IItemInventoryRet DoGetInventory( string icode )
		{
			var InvQ = NewRequestSet.AppendItemInventoryQueryRq();
			InvQ.ORListQueryWithOwnerIDAndClass.FullNameList.Add( icode );

			var ResponseSet = DoRequests( null );
			if( ResponseSet != null )
			{
				var Response = ResponseSet.ResponseList.GetAt( 0 );
				if( Response != null )
				{
					var InventoryRetList = Response.Detail as IItemInventoryRetList;
					if( InventoryRetList != null && InventoryRetList.Count > 0 )
						return InventoryRetList.GetAt( 0 );
				}
				return null;
			}
			throw new Exception( "DoGetInventory (No Response)" );
		}


		/*
		private Inventory GetInventory( string icode )
		{
			if( !HasInventory( icode ) ) // Force Get Of Item
				return null;

			lock( InventoryCodes )
			{
				return InventoryCodes[ icode ];
			}
		}
		*/

		private bool HasInventory( string icode )
		{
			icode = icode.Trim();

			lock( InventoryCodes )
			{
				if( !InventoryCodes.ContainsKey( icode ) )
				{
					var Item = DoGetInventory( icode );
					if( Item != null )
						InventoryCodes.Add( icode, new Inventory( Item ) );
					else
						return false;
				}
			}
			return true;
		}

		private IItemInventoryRet DoAddUpdateInventory( bool add, string icode, string description, string taxCode,
														string ledger )
		{
			if( add )
			{
				var InvQ = NewRequestSet.AppendItemInventoryAddRq();
				InvQ.Name.SetValue( icode );
				InvQ.ManufacturerPartNumber.SetValue( icode );
				InvQ.SalesDesc.SetValue( description );
				InvQ.PurchaseDesc.SetValue( description );
				InvQ.SalesTaxCodeRef.FullName.SetValue( taxCode );

				InvQ.IncomeAccountRef.FullName.SetValue( ledger );
				InvQ.AssetAccountRef.FullName.SetValue( AssetLedger );
				InvQ.COGSAccountRef.FullName.SetValue( CostOfGoodsLedger );


				InvQ.IsActive.SetValue( true );
				InvQ.IsTaxIncluded.SetValue( false );
				InvQ.SalesPrice.SetValue( 0 );
			}
			else
			{
				var Inv = DoGetInventory( icode );
				var InvQ = NewRequestSet.AppendItemInventoryModRq();
				InvQ.ListID.SetValue( Inv.ListID.GetValue() );
				InvQ.EditSequence.SetValue( Inv.EditSequence.GetValue() );
				InvQ.Name.SetValue( icode );
				InvQ.SalesDesc.SetValue( description );
				InvQ.PurchaseDesc.SetValue( description );
				InvQ.SalesTaxCodeRef.FullName.SetValue( taxCode );
			}

			DoRequests( ( add ? "Cannot create inventory item:" : "Cannot modify inventory item:" ) + icode );
			return DoGetInventory( icode );
		}


		private void AddUpdateInventory( string icode, string description, string taxCode, string ledger )
		{
			icode = icode.Trim();
			taxCode = taxCode.Length > 1 ? taxCode.Substring( 0, 1 ) : "G";

			lock( InventoryCodes )
			{
				if( !HasInventory( icode ) )
					InventoryCodes.Add( icode, new Inventory( DoAddUpdateInventory( true, icode, description, taxCode, ledger ) ) );
				else
				{
					var Inv = InventoryCodes[ icode ];
					if( Inv.Description != description || Inv.TaxCode != taxCode )
						InventoryCodes[ icode ] = new Inventory( DoAddUpdateInventory( false, icode, description, taxCode, ledger ) );
				}
			}
		}
	}
}