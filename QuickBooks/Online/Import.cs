﻿using System;
using System.Collections.Generic;
using Intuit.Ipp.Data;
using IppDotNetSdkQuickBooks;
using Row = IdsImport.Row;

namespace IdsQuickBooks.QuickBooks.Online
{
	public partial class Import : IdsImport.Import
	{
		private RestHelper.Context Context;

		private Invoice CurrentInvoice;
		private List<Line> CurrentInvoiceLines;
		private Customer CurrentCustomer;
//		private string CurrentInvoiceNumber = "";

		private decimal TotalTax;

		protected override void OnDispose( bool systemDisposing )
		{
		}

		protected override void OnException( Row csvRow, Exception exception )
		{
		}

		protected override void BeginUpdate( string errorPath, bool debug )
		{
			RestHelper.DebugLogPath = errorPath;
			RestHelper.Debug = debug;

			Context = new RestHelper.Context();
		}

		protected override void EndUpdate( bool errors )
		{
			Context = null;
		}

		protected override void BeginInvoice( string accountNumber, string invoiceNumber, DateTime invoiceDate, string taxCode,
			bool exempt, Row accountLineRow )
		{
//			CurrentInvoiceNumber = invoiceNumber;
			TotalTax = 0;

			Context.DefaultTaxCode = ( exempt ? "EXEMPT" : taxCode );

			string CompanyName = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_COMPANY ];
			string Address1 = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_ADDRESS_1 ];
			string Address2 = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_ADDRESS_2 ];
			string City = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_CITY ];
			string Region = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_REGION ];
			string PostCode = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_POST_CODE ];
			string Country = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_COUNTRY ];
			string Contact = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_CONTACT ];
			string Phone = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_TELEPHONE ];
			string Email = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.BILLING_EMAIL ];

			CurrentCustomer = AddUpdateCustomer( accountNumber, taxCode, CompanyName,
				Address1, Address2,
				City, Region, PostCode, Country,
				Contact, Phone, Email );

//			CompanyName = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.SHIPPING_COMPANY ];
			Address1 = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.SHIPPING_ADDRESS_1 ];
			Address2 = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.SHIPPING_ADDRESS_2 ];
			City = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.SHIPPING_CITY ];
			Region = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.SHIPPING_REGION ];
			PostCode = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.SHIPPING_POST_CODE ];
			Country = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.SHIPPING_COUNTRY ];
			Contact = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.SHIPPING_CONTACT ];
			Phone = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.SHIPPING_TELEPHONE ];
			Email = accountLineRow[ (int) IdsImport.AccountLine.FIELDS.SHIPPING_EMAIL ];

			CurrentInvoice = RestHelper.NewInvoice( CurrentCustomer, invoiceDate, invoiceNumber );

			CurrentInvoice.ShipAddr = RestHelper.NameAndAddress( ref Address1, ref Address2,
				City, Region, PostCode,
				Country, Contact, Phone, Email );
			CurrentInvoice.BillAddr = CurrentCustomer.BillAddr;

			CurrentInvoiceLines = new List<Line>();
		}

		protected override void EndInvoice( bool errorThrown )
		{
			if( !errorThrown )
			{
				CurrentInvoice.Line = CurrentInvoiceLines.ToArray();
				Context.NewTaxDetailLine( CurrentInvoice, TotalTax );

				var Invoice = Context.AddInvoice( CurrentInvoice );

				if( Invoice.TotalAmt != CurrentInvoice.TotalAmt )
				{
					AddUpdateInventory( "ROUNDING", "Rounding" );

					var Rounding = GetInventory( "ROUNDING" );

					var Diff = CurrentInvoice.TotalAmt - Invoice.TotalAmt;

					var CurrentLine = RestHelper.NewInvoiceLine( "Rounding", Diff );

					Context.NewInvoiceLineDetail( CurrentLine, Rounding, Diff );
					CurrentInvoiceLines.Add( CurrentLine );

					var NewInv = new Invoice
					{
						Id = Invoice.Id,
						SyncToken = Invoice.SyncToken,
						CustomerRef = Invoice.CustomerRef,
						DocNumber = Invoice.DocNumber,
						Line = CurrentInvoiceLines.ToArray()
					};

					Context.UpdateInvoice( NewInv );
				}
			}
			CurrentInvoice = null;
			CurrentCustomer = null;
		}


		protected override void TripLine( Row csvRow )
		{
			var InventoryKey = TripInventoryKey( csvRow );
			AddUpdateInventory( InventoryKey, "Item: " + InventoryKey );

			var TripId = "Trip ID: " + csvRow[ (int) IdsImport.TripLine.FIELDS.TRIP_ID ];
			var Amount = (decimal) csvRow[ (int) IdsImport.TripLine.FIELDS.BASE_RATE ].AsDouble;
			var ICode = InventoryCodes[ InventoryKey ];

			Line CurrentLine;

			if( Amount == 0 )
				CurrentLine = RestHelper.NewCommentLine( TripId );
			else
			{
				CurrentLine = RestHelper.NewInvoiceLine( TripId, Amount );
				Context.NewInvoiceLineDetail( CurrentLine, ICode, Amount );
			}

			CurrentInvoiceLines.Add( CurrentLine );

			var Discount = -(decimal) csvRow[ (int) IdsImport.TripLine.FIELDS.DISCOUNT_AMOUNT ].AsDouble;
			if( Discount != 0 )
			{
				CurrentLine = RestHelper.NewInvoiceLine( "Discount", Discount );
				CurrentInvoiceLines.Add( CurrentLine );
				Context.NewInvoiceLineDetail( CurrentLine, ICode, Discount );
			}
		}

		protected override void TaxLine( Row csvRow, double amount )
		{
			TotalTax += (decimal) amount;
		}

		protected override void ChargeLine( Row csvRow, double amount )
		{
			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if( amount != 0 )
			{
				var Amount = (decimal) amount;
				var InventoryKey = ChargeInventoryKey( csvRow );
				var Charge = csvRow[ (int) IdsImport.ChargeLine.FIELDS.DESCRIPTION ].AsString.Trim();

				AddUpdateInventory( InventoryKey, "Charge: " + Charge );

				var ChargeLine = RestHelper.NewInvoiceLine( "Charge: " + Charge, Amount );
				CurrentInvoiceLines.Add( ChargeLine );
				Context.NewInvoiceLineDetail( ChargeLine, InventoryCodes[ InventoryKey ], Amount );
			}
		}


		protected override void AccountLine( Row csvRow )
		{
		}

		protected override void EndOfInvoiceLine( Row csvRow, bool errorThrown )
		{
			CurrentInvoice.TotalAmt = (decimal) csvRow[ (int) IdsImport.EndOfInvoiceLine.FIELDS.INVOICE_TOTAL ].AsDouble;
		}
	}
}