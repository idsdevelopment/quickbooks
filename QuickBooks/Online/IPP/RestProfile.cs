﻿namespace IppDotNetSdkQuickBooks
{
	public static class Profile
	{
		public static string RealmId
		{
			get { return Constants.Consumer.RelmId; }
		}

		public static string OAuthAccessToken
		{
			get { return Constants.AccessToken.Key; }
			set { Constants.AccessToken.Key = value; }
		}

		public static string OAuthAccessSecret
		{
			get { return Constants.AccessToken.Secret; }
			set { Constants.AccessToken.Secret = value; }
		}
	}
}