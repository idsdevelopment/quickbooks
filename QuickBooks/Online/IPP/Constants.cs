﻿using IdsQuickBooks.Properties;

namespace IppDotNetSdkQuickBooks
{
	public class Constants
	{
/*
		public static class Application
		{
			public const string Key = "0b6d9425bd237b4cd2bb427b4c97dbfcf912";
		}
*/

		public static class Company
		{
			public static string Id
			{
				get { return Settings.Default.CompanyId; }
			}

			public static string UserName
			{
				get { return Settings.Default.UserName; }
			}
		}

		// For Renewal ??
		public static class Consumer
		{
			public const string Key = "qyprdu3qBS8kkfPpJYZxeVwo85Ds1o";
			public const string Secret = "dqnK6NfifJwSAc8VdenzHrcuaoKa1pEvVxFoEEbK";

			// Not Currently Used ??
			public static string RelmId
			{
				get { return Settings.Default.CompanyId; }
			}
		}

		public static class AccessToken
		{
			public static string Key
			{
				get { return Settings.Default.AccessToken.Trim(); }

				set
				{
					var D = Settings.Default;
					D.AccessToken = value;
					D.Save();
				}
			}

			public static string Secret
			{
				get { return Settings.Default.AccessSecret.Trim(); }

				set
				{
					var D = Settings.Default;
					D.AccessSecret = value;
					D.Save();
				}
			}
		}


		/// <summary>
		///     OAuth EndPoints.
		/// </summary>
		public static class OauthEndPoints
		{
			/// <summary>
			///     Url Request Token
			/// </summary>
			public static string UrlRequestToken = "/get_request_token";

			/// <summary>
			///     Url Access Token
			/// </summary>
			public static string UrlAccessToken = "/get_access_token";

			/// <summary>
			///     Federation base url.
			/// </summary>
			public static string IdFedOAuthBaseUrl = "https://oauth.intuit.com/oauth/v1";

			/// <summary>
			///     Authorize url.
			/// </summary>
			public static string AuthorizeUrl = "https://workplace.intuit.com/Connect/Begin";
		}


		/// <summary>
		///     IPP Platform Endpoints
		/// </summary>
		public static class IppEndPoints
		{
			/// <summary>
			///     BlueDot Menu Url.
			/// </summary>
			public static string BlueDotAppMenuUrl = "https://workplace.intuit.com/api/v1/Account/AppMenu";

			/// <summary>
			///     Disconnect url.
			/// </summary>
			public static string DisconnectUrl = "https://appcenter.intuit.com/api/v1/Connection/Disconnect";
		}
	}
}