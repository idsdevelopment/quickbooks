﻿using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;
using IppDotNetSdkQuickBooks;

namespace IppDotNetSdkQuickBooksApiV3.OAuth
{
	public class Grant
	{
		/// <summary>
		///     Creates Session
		/// </summary>
		/// <returns>Returns OAuth Session</returns>
		protected IOAuthSession CreateSession( string consumerKey, string consumerSecret, string oauthEndpoint )
		{
			var consumerContext = new OAuthConsumerContext
			{
				ConsumerKey = consumerKey,
				ConsumerSecret = consumerSecret,
				SignatureMethod = SignatureMethod.HmacSha1
			};

			return new OAuthSession( consumerContext,
				Constants.OauthEndPoints.IdFedOAuthBaseUrl + Constants.OauthEndPoints.UrlRequestToken,
				oauthEndpoint,
				Constants.OauthEndPoints.IdFedOAuthBaseUrl + Constants.OauthEndPoints.UrlAccessToken );
		}
	}
}