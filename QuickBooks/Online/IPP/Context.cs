﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using DevDefined.OAuth.Consumer;
using DevDefined.OAuth.Framework;
using IDS_InvoiceExport;
using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Exception;
using Intuit.Ipp.LinqExtender;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.Security;

namespace IppDotNetSdkQuickBooks
{
	public static partial class RestHelper
	{
		public class Context : ServiceContext
		{
			private readonly DataService DataService;
			private readonly QueryService<Account> AccountQueryService;
			private readonly QueryService<Customer> CustomerQueryService;
			private readonly QueryService<Item> InventoryQueryService;
			private readonly QueryService<TaxCode> TaxQueryService;
			// ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
//			private readonly QueryService<CompanyInfo> CompanyQueryService;
			private readonly QueryService<Invoice> InvoiceQueryService;

//			private CompanyInfo CompanyData;

			private ReferenceType Sales;
			public Dictionary<string, TaxCode> TaxCodes = new Dictionary<string, TaxCode>();

			// ReSharper disable once CollectionNeverQueried.Local
			private readonly Dictionary<string, Account> Ledgers = new Dictionary<string, Account>();

			public string DefaultTaxCode;

			public Context()
				: base( Profile.RealmId,
					IntuitServicesType.QBO,
					new OAuthRequestValidator( Profile.OAuthAccessToken, Profile.OAuthAccessSecret,
						Constants.Consumer.Key, Constants.Consumer.Secret ) )
			{
#if DEBUG
				IppConfiguration.BaseUrl.Qbo = "https://sandbox-quickbooks.api.intuit.com/";
#else
				IppConfiguration.BaseUrl.Qbo = "https://quickbooks.api.intuit.com/";
#endif
				if( Debug )
				{
					IppConfiguration.Logger.RequestLog.EnableRequestResponseLogging = true;

					var Path = Utils.AddPathSeparator( DebugLogPath ) + @"QuickBooks\Logs";

					if( !Directory.Exists( Path ) )
						Directory.CreateDirectory( Path );

					IppConfiguration.Logger.RequestLog.ServiceRequestLoggingLocation = Utils.AddPathSeparator( DebugLogPath )
																					   + @"QuickBooks\Logs";
				}
//				else
//					IppConfiguration.RetryPolicy = new Intuit.Ipp.Retry.IntuitRetryPolicy( 500, new TimeSpan( 0, 0, 1 ), new TimeSpan( 0, 1, 0 ), new TimeSpan( 0, 0, 3 ) );


				DataService = new DataService( this );
				AccountQueryService = new QueryService<Account>( this );
				CustomerQueryService = new QueryService<Customer>( this );
				InventoryQueryService = new QueryService<Item>( this );
				TaxQueryService = new QueryService<TaxCode>( this );
//				CompanyQueryService = new QueryService<CompanyInfo>( this );
				InvoiceQueryService = new QueryService<Invoice>( this );

//				CompanyData = ( from Co in CompanyQueryService
//								select Co ).FirstOrDefault();

				foreach( var Acc in GetAccounts() )
					Ledgers.Add( Acc.Name, Acc );

				( from Tc in TaxQueryService select Tc ).ToList().ForEach( Tc => TaxCodes.Add( Tc.Name.ToUpper().Trim(), Tc ) );

				DefaultTaxCode = "GST";

				// Auto Reconnect
				var ReconnetString = ReconnectRealm();
				using( var Reader = XmlReader.Create( new StringReader( ReconnetString ) ) )
				{
					while( Reader.Read() )
					{
						if( Reader.NodeType == XmlNodeType.Element )
						{
							switch( Reader.Name )
							{
							case "ErrorMessage":
								Reader.Read();
								if( Reader.Value.ToUpper().IndexOf( "OUT OF BOUNDS", StringComparison.Ordinal ) >= 0 ) // Hasn't expired
									return;
								throw new Exception( "Authorisation Error: " + Reader.Value );

							case "OAuthToken":
								Reader.Read();
								Profile.OAuthAccessToken = Reader.Value.Trim();
								break;

							case "OAuthTokenSecret":
								Reader.Read();
								Profile.OAuthAccessSecret = Reader.Value.Trim();
								break;
							}
						}
					}
				}
			}


			private static string GetDevDefinedOAuthHeader( HttpWebRequest webRequest, string consumerKey, string consumerSecret,
				string accessToken, string accessTokenSecret )
			{
				var consumerContext = new OAuthConsumerContext
				{
					ConsumerKey = consumerKey,
					ConsumerSecret = consumerSecret,
					SignatureMethod = SignatureMethod.HmacSha1,
					UseHeaderForOAuthParameters = true
				};

				consumerContext.UseHeaderForOAuthParameters = true;

				//URIs not used - we already have Oauth tokens
				var oSession = new OAuthSession( consumerContext,
					"https://www.example.com",
					"https://www.example.com",
					"https://www.example.com" );

				oSession.AccessToken = new TokenBase
				{
					Token = accessToken,
					ConsumerKey = consumerKey,
					TokenSecret = accessTokenSecret
				};

				var consumerRequest = oSession.Request();
				consumerRequest = consumerRequest.ForMethod( webRequest.Method );
				consumerRequest = consumerRequest.ForUri( webRequest.RequestUri );
				consumerRequest = consumerRequest.SignWithToken();
				return consumerRequest.Context.GenerateOAuthParametersForHeader();
			}


			public static string DisconnectRealm( string consumerKey, string consumerSecret, string accessToken,
				string accessTokenSecret )
			{
				var httpWebRequest =
					WebRequest.Create( "https://appcenter.intuit.com/api/v1/Connection/Disconnect" ) as HttpWebRequest;
				if( httpWebRequest != null )
				{
					httpWebRequest.Method = "GET";
					httpWebRequest.Headers.Add( "Authorization",
						GetDevDefinedOAuthHeader( httpWebRequest, consumerKey, consumerSecret, accessToken, accessTokenSecret ) );
					var httpWebResponse = httpWebRequest.GetResponse() as HttpWebResponse;
					if( httpWebResponse != null )
					{
						var data = httpWebResponse.GetResponseStream();
						if( data != null )
							using( data )
							{
								//return XML response
								return new StreamReader( data ).ReadToEnd();
							}
					}
				}
				return null;
			}

			public string ReconnectRealm( string consumerKey, string consumerSecret, string accessToken, string accessTokenSecret )
			{
				var httpWebRequest =
					WebRequest.Create( "https://appcenter.intuit.com/api/v1/Connection/Reconnect" ) as HttpWebRequest;

				if( httpWebRequest != null )
				{
					httpWebRequest.Method = "GET";
					httpWebRequest.Headers.Add( "Authorization",
						GetDevDefinedOAuthHeader( httpWebRequest, consumerKey, consumerSecret, accessToken, accessTokenSecret ) );
					var httpWebResponse = httpWebRequest.GetResponse() as HttpWebResponse;
					if( httpWebResponse != null )
					{
						var data = httpWebResponse.GetResponseStream();
						if( data != null )
						{
							using( data )
							{
								return new StreamReader( data ).ReadToEnd();
							}
						}
					}
				}
				return null;
			}

			public string ReconnectRealm()
			{
				return ReconnectRealm( Constants.Consumer.Key, Constants.Consumer.Secret, Profile.OAuthAccessToken,
					Profile.OAuthAccessSecret );
			}

			private TaxCode FindTaxCode( string taxCode )
			{
				taxCode = taxCode.Trim().ToUpper();

				foreach( var Tc in TaxCodes )
				{
					if( Tc.Key.StartsWith( taxCode ) )
						return Tc.Value;
				}
				return null;
			}

			private TaxCode FindTaxCodeById( string id )
			{
				foreach( var Tc in TaxCodes )
				{
					var Value = Tc.Value;

					if( Value.Id == id )
						return Value;
				}
				return FindTaxCode( DefaultTaxCode );
			}

			public TaxCode GetExemptTaxCode()
			{
				var Tc = FindTaxCode( "EXEMPT" );
				if( Tc != null )
					return Tc;

				Tc = FindTaxCode( "NON" );
				return Tc;
			}

			public bool IsExempt( string taxCode )
			{
				taxCode = taxCode.Trim().ToUpper();
				return ( taxCode == "EXEMPT" || taxCode == "NON" );
			}

			public TaxCode GetTaxCode( string taxCode )
			{
				var Tc = FindTaxCode( taxCode );
				if( Tc != null )
					return Tc;

				Tc = FindTaxCode( "GST" );
				if( Tc != null )
					return Tc;

				Tc = FindTaxCode( "HST" );
				if( Tc != null )
					return Tc;

				Tc = FindTaxCode( "PST" );
				if( Tc != null )
					return Tc;

				Tc = FindTaxCode( "TAX" );
				if( Tc != null )
					return Tc;

				Tc = FindTaxCode( "VAT" );
				if( Tc != null )
					return Tc;

				Tc = FindTaxCode( "EXEMPT" );
				if( Tc != null )
					return Tc;

				Tc = FindTaxCode( "NON" );
				if( Tc != null )
					return Tc;

				Tc = GetExemptTaxCode();

				if( Tc != null )
					return Tc;

				throw new Exception( "Missing Tax Rate: " + taxCode );
			}

			public List<Account> GetAccounts()
			{
				var Accounts = ( from Acc in AccountQueryService
								 select Acc ).ToList();

				foreach( var Acc in Accounts )
				{
					if( Acc.Name.ToUpper().Trim() == "SALES" )
					{
						Sales = new ReferenceType { name = Acc.Name, Value = Acc.Id };
						break;
					}
				}
				return Accounts;
			}

			public List<TaxCode> GetTaxCodes()
			{
				return ( from Tax in TaxQueryService
						 select Tax ).ToList();
			}


			public Customer GetCustomer( string idsAccountNumber )
			{
				return ( from C in CustomerQueryService
						 where C.DisplayName == idsAccountNumber
						 select C ).FirstOrDefault();
			}

			public Customer AddUpdateCustomer( string idsAccountNumber, string taxCode, string companyName,
				string address1, string address2,
				string city, string region, string postCode,
				string country, string contact, string phone, string email )
			{
				idsAccountNumber = idsAccountNumber.Trim();
				companyName = companyName.Trim();

				// Fixes Up Address
				var BillAddr = NameAndAddress( ref address1, ref address2, city, region, postCode,
					country, contact, phone, email );

				var Cust = GetCustomer( idsAccountNumber );
				var Add = ( Cust == null );

				if( Add )
					Cust = new Customer();

				// Check For Change
				else if( Cust.ContactName.IsEqualOrNull( companyName ) )
				{
					var Adr = Cust.BillAddr;

					if( Adr.City.IsEqualOrNull( city ) && Adr.Country.IsEqualOrNull( country )
						&& Adr.Line1.IsEqualOrNull( address1 ) && Adr.Line2.IsEqualOrNull( address2 )
						&& Adr.CountrySubDivisionCode.IsEqualOrNull( region ) && Adr.PostalCode.IsEqualOrNull( postCode ) )
					{
						if( Cust.ContactName.IsEqualOrNull( contact ) )
						{
							if( ( Cust.PrimaryPhone == null && phone == "" ) || Cust.PrimaryPhone.FreeFormNumber.IsEqualOrNull( phone ) )
							{
								if( ( Cust.PrimaryEmailAddr == null && email == "" ) || Cust.PrimaryEmailAddr.Address.IsEqualOrNull( email ) )
									return Cust;
							}
						}
					}

					// Has to be done through new Customer (Bug in SDK)
					var UCust = new Customer
					{
						Id = Cust.Id,
						SyncToken = Cust.SyncToken
					};
					Cust = UCust;
				}

				Cust.DisplayName = idsAccountNumber;
				Cust.CompanyName = companyName;
				Cust.BillAddr = BillAddr;

				Cust.ContactName = contact.Trim();
				Cust.PrimaryPhone = new TelephoneNumber { Default = true, FreeFormNumber = phone.Trim() };
				Cust.PrimaryEmailAddr = new EmailAddress { Address = email.Trim() };

				Cust.Taxable = true;
				Cust.TaxableSpecified = true;

				if( Add )
				{
					var Tx = GetTaxCode( taxCode );
					Cust.DefaultTaxCodeRef = new ReferenceType { name = Tx.Name, Value = Tx.Id };
					Cust = DataService.Add( Cust );
				}
				else
					Cust = DataService.Update( Cust );

				return Cust;
			}

			public Item FixInventory( Item item )
			{
				if( item != null )
				{
					var Tax = FindTaxCodeById( item.SalesTaxCodeRef.Value );

					item.SalesTaxCodeRef = new ReferenceType { name = Tax.Name, Value = Tax.Id };
					item.Taxable = !IsExempt( Tax.Name );
				}
				return item;
			}

			public Item GetInventory( string itemCode )
			{
				var RetVal = ( from I in InventoryQueryService
							   where I.Name == itemCode
							   select I ).FirstOrDefault();

				return FixInventory( RetVal );
			}


			public Item AddUpdateInventory( string itemCode, string description )
			{
				var I = GetInventory( itemCode );

				var Add = ( I == null );

				if( Add )
					I = new Item();

				else if( !I.Description.IsEqualOrNull( description ) || I.IncomeAccountRef == null )
				{
					var Ui = new Item
					{
						Id = I.Id,
						SyncToken = I.SyncToken
					};
					I = Ui;
				}
				else
					return I;

				I.Type = ItemTypeEnum.Inventory;
				I.Name = itemCode.Trim();
				I.Description = description.Trim();
				I.IncomeAccountRef = Sales;

				if( Add )
				{
					I.Taxable = true;

					var Tx = GetTaxCode( "GST" );
					I.SalesTaxCodeRef = new ReferenceType { name = Tx.Name, Value = Tx.Id };
					I = FixInventory( DataService.Add( I ) );
				}
				else
					I = FixInventory( DataService.Update( I ) );

				return I;
			}

			public Line NewInvoiceLineDetail( Line line, Item inventoryItem, decimal amount )
			{
				TaxCode Tx;
				if( inventoryItem.Taxable )
					Tx = GetTaxCode( DefaultTaxCode );
				else
					Tx = GetExemptTaxCode();

				line.AnyIntuitObject = new SalesItemLineDetail
				{
					ItemRef = new ReferenceType { name = inventoryItem.Name, Value = inventoryItem.Id },
					AnyIntuitObject = amount,
					ItemElementName = ItemChoiceType.UnitPrice,
					Qty = 1,
					QtySpecified = true,
					TaxInclusiveAmt = amount,
					TaxCodeRef = new ReferenceType { name = Tx.Name, Value = Tx.Id }
				};
				return line;
			}

			public Invoice NewTaxDetailLine( Invoice invoice, decimal totalTax )
			{
				var TxTaxDetail = new TxnTaxDetail { TotalTax = totalTax, TotalTaxSpecified = true };

				var TaxLine = new Line { DetailType = LineDetailTypeEnum.TaxLineDetail };
				var TaxDetail = new TaxLineDetail();

				var Tax = GetTaxCode( DefaultTaxCode );

				TaxDetail.TaxRateRef = new ReferenceType { name = Tax.Name, Value = Tax.Id };
				TaxLine.AnyIntuitObject = TaxDetail;
				TxTaxDetail.TaxLine = new[] { TaxLine };
				invoice.TxnTaxDetail = TxTaxDetail;

				return invoice;
			}

			public Invoice GetInvoice( string accountId, string invNumber )
			{
				var RetVal = ( from I in InvoiceQueryService
							   where I.DocNumber == invNumber
							   select I ).ToArray();

				foreach( var Rec in RetVal )
				{
					if( Rec.CustomerRef.name == accountId )
						return Rec;
				}
				return null;
			}


			public Invoice AddInvoice( Invoice invoice )
			{
				var Retry = 2;
				while( true )
				{
					try
					{
						return DataService.Add( invoice );
					}
					catch( RetryExceededException )
					{
						if( Retry-- <= 0 )
							throw;
					}
				}
			}


			public Invoice UpdateInvoice( Invoice invoice )
			{
				invoice.sparseSpecified = true;
				var Retry = 2;
				while( true )
				{
					try
					{
						try
						{
							return DataService.Update( invoice );
						}
						catch
						{
						}
					}
					catch( RetryExceededException )
					{
						if( Retry-- <= 0 )
							throw;
					}
				}
			}
		}
	}
}