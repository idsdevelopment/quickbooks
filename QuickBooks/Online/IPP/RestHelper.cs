﻿using System;
using Intuit.Ipp.Data;

namespace IppDotNetSdkQuickBooks
{
	public static partial class RestHelper
	{
		public static bool Debug = false;
		public static string DebugLogPath = "";

		public static PhysicalAddress NameAndAddress( ref string address1, ref string address2,
			string city, string region, string postCode,
			string country, string contact, string phone, string email )
		{
			if( address1.Trim() == "" )
			{
				address1 = address2.Trim();
				address2 = "";
			}

			return new PhysicalAddress
			{
				City = city.Trim(),
				Country = country.Trim(),
				CountryCode = "",
				CountrySubDivisionCode = region.Trim(),
				Line1 = address1.Trim(),
				Line2 = address2.Trim(),
				PostalCode = postCode.Trim()
			};
		}


		public static Invoice NewInvoice( Customer cust, DateTime invoiceDate, string invoiceNumber )
		{
			return new Invoice
			{
				CustomerRef = new ReferenceType { name = cust.DisplayName, Value = cust.Id },
				AutoDocNumber = false,
				DocNumber = invoiceNumber,
				TxnDate = invoiceDate,
				TxnDateSpecified = true
			};
		}

		public static Line NewInvoiceLine( string description )
		{
			return new Line
			{
				Description = description
			};
		}

		public static Line NewInvoiceLine( string description, decimal amount )
		{
			return new Line
			{
				Description = description,
				Amount = amount,
				AmountSpecified = true,
				DetailType = LineDetailTypeEnum.SalesItemLineDetail,
				DetailTypeSpecified = true
			};
		}

		public static Line NewCommentLine( string comment )
		{
			return new Line
			{
				Description = comment,
				AmountSpecified = false,
				DetailType = LineDetailTypeEnum.DescriptionOnly,
				DetailTypeSpecified = true
			};
		}

		public static Line NewDiscountLine( decimal amount )
		{
			return new Line
			{
				Description = "Discount",
				Amount = amount,
				AmountSpecified = true,
				DetailType = LineDetailTypeEnum.DiscountLineDetail,
				DetailTypeSpecified = true,
				AnyIntuitObject = new DiscountLineDetail
				{
					PercentBased = false,
					PercentBasedSpecified = false
				}
			};
		}
	}
}