﻿using System.Collections.Generic;
using Intuit.Ipp.Data;

namespace IdsQuickBooks.QuickBooks.Online
{
	public partial class Import
	{
		private readonly Dictionary<string, Item> InventoryCodes = new Dictionary<string, Item>();

		private Item GetInventory( string icode )
		{
			if( !HasInventory( icode ) ) // Force Get Of Item
				return null;

			lock( InventoryCodes )
			{
				return InventoryCodes[ icode ];
			}
		}

		private bool HasInventory( string icode )
		{
			icode = icode.Trim();

			lock( InventoryCodes )
			{
				if( !InventoryCodes.ContainsKey( icode ) )
				{
					var Item = Context.GetInventory( icode );
					if( Item != null )
						InventoryCodes.Add( icode, Item );
					else
						return false;
				}
				return true;
			}
		}

		private void AddUpdateInventory( string icode, string description )
		{
			icode = icode.Trim();

			lock( InventoryCodes )
			{
				if( !HasInventory( icode ) )
					InventoryCodes.Add( icode, Context.AddUpdateInventory( icode, description ) );
				else if( InventoryCodes[ icode ].Description != description )
					InventoryCodes[ icode ] = Context.AddUpdateInventory( icode, description );
			}
		}
	}
}