﻿using System.Collections.Generic;
using Intuit.Ipp.Data;

namespace IdsQuickBooks.QuickBooks.Online
{
	public partial class Import
	{
		private readonly Dictionary<string, Customer> CustomerCodes = new Dictionary<string, Customer>();

		private bool HasCustomer( string custCode )
		{
			lock( CustomerCodes )
			{
				custCode = custCode.Trim();

				if( !CustomerCodes.ContainsKey( custCode ) )
				{
					var Item = Context.GetCustomer( custCode );
					if( Item != null )
						CustomerCodes.Add( custCode, Item );
					else
						return false;
				}
				return true;
			}
		}


		public Customer AddUpdateCustomer( string idsAccountNumber, string taxCode, string companyName,
			string address1, string address2,
			string city, string region, string postCode,
			string country, string contact, string phone, string email )
		{
			idsAccountNumber = idsAccountNumber.Trim();

			if( !HasCustomer( idsAccountNumber ) )
			{
				var Cust = Context.AddUpdateCustomer( idsAccountNumber, taxCode, companyName,
					address1, address2,
					city, region, postCode, country,
					contact, phone, email );
				lock( CustomerCodes )
					CustomerCodes.Add( idsAccountNumber, Cust );
			}
			lock( CustomerCodes )
			{
				return CustomerCodes[ idsAccountNumber ];
			}
		}
	}
}