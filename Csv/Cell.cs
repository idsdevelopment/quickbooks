﻿using System.Globalization;

namespace IdsImport
{
	public class Cell
	{
		private string _StringContent = "";
		private double _NumericContent;
		private bool IsDouble;

		public static implicit operator string( Cell c )
		{
			return c.AsString;
		}

		public static implicit operator Cell( string s )
		{
			return new Cell { _StringContent = s, _NumericContent = 0, IsDouble = false };
		}

		public static implicit operator double( Cell c )
		{
			return c.AsDouble;
		}

		public static implicit operator Cell( double d )
		{
			return new Cell { _StringContent = "", _NumericContent = d, IsDouble = true };
		}

		private double ToDouble()
		{
			if( !IsDouble )
			{
				IsDouble = true;
				if( !double.TryParse( _StringContent, out _NumericContent ) )
					_NumericContent = 0;
			}
			return _NumericContent;
		}


		private new string ToString()
		{
			if( IsDouble )
			{
				IsDouble = false;
				_StringContent = _NumericContent.ToString( CultureInfo.InvariantCulture );
			}
			return _StringContent;
		}


		public string AsString
		{
			get { return ToString(); }

			set
			{
				IsDouble = false;
				_StringContent = value;
			}
		}

		public double AsDouble
		{
			get { return ToDouble(); }

			set
			{
				IsDouble = true;
				_NumericContent = value;
			}
		}

		internal string AsFileString
		{
			get
			{
				var SaveIsDouble = IsDouble;

				var Value = AsString;
				Value = Value.Replace( "\"", "\"\"" );
				IsDouble = SaveIsDouble;
				return SaveIsDouble ? Value : "\"" + Value + "\"";
			}
		}
	}
}