﻿namespace IdsImport
{
	public static class ChargeLine
	{
		public enum FIELDS
		{
			ACCOUNT_ID = 1,
			INVOICE_NUMBER,
			TRIP_ID,

			DESCRIPTION,
			VALUE
		}

		public static string InventoryKey( Row csvRow )
		{
			return "CHARGE_" + csvRow[ (int) FIELDS.DESCRIPTION ].AsString.Trim();
		}
	}
}