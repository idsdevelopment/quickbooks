﻿namespace IdsQuickBooks
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.IncludeCharges = new System.Windows.Forms.CheckBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.DesktopRadioButton = new System.Windows.Forms.RadioButton();
			this.OnlineRadioButton = new System.Windows.Forms.RadioButton();
			this.label14 = new System.Windows.Forms.Label();
			this.InventoryCodeStructure = new System.Windows.Forms.ComboBox();
			this.button1 = new System.Windows.Forms.Button();
			this.ErrorFolder = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.button3 = new System.Windows.Forms.Button();
			this.IDSExportPath = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.OptionsTabControl = new System.Windows.Forms.TabControl();
			this.OnlineTabPage = new System.Windows.Forms.TabPage();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.label4 = new System.Windows.Forms.Label();
			this.CompanyId = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.AccessTokenKey = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.AccessTokenSecret = new System.Windows.Forms.TextBox();
			this.DesktopTabPage = new System.Windows.Forms.TabPage();
			this.DefaultInvoiceLayout = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.FuelSurchargeCode = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.FuelSurchargeLedger = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.DiscountItemCode = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.CostOfGoodsLedger = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.AssetLedger = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.SalesLedger = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.button2 = new System.Windows.Forms.Button();
			this.QuickbooksCompanyFile = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.UserName = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.Password = new System.Windows.Forms.Label();
			this.TrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
			this.IconTrayMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.showToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.fileToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
			this.PollTimer = new System.Windows.Forms.Timer(this.components);
			this.FolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.HideButton = new System.Windows.Forms.Button();
			this.fileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.showToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.Debug = new System.Windows.Forms.CheckBox();
			this.ImportBtn = new System.Windows.Forms.Button();
			this.MainMenu = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.autoHideOnStartupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.OptionsTabControl.SuspendLayout();
			this.OnlineTabPage.SuspendLayout();
			this.DesktopTabPage.SuspendLayout();
			this.IconTrayMenuStrip.SuspendLayout();
			this.MainMenu.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.IncludeCharges);
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Controls.Add(this.label14);
			this.groupBox1.Controls.Add(this.InventoryCodeStructure);
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Controls.Add(this.ErrorFolder);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.button3);
			this.groupBox1.Controls.Add(this.IDSExportPath);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.OptionsTabControl);
			this.groupBox1.Location = new System.Drawing.Point(22, 38);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(523, 529);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = " Settings ";
			// 
			// IncludeCharges
			// 
			this.IncludeCharges.AutoSize = true;
			this.IncludeCharges.Location = new System.Drawing.Point(237, 62);
			this.IncludeCharges.Name = "IncludeCharges";
			this.IncludeCharges.Size = new System.Drawing.Size(218, 17);
			this.IncludeCharges.TabIndex = 12;
			this.IncludeCharges.Text = "Include Charges In Inventory Item Value ";
			this.IncludeCharges.UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.DesktopRadioButton);
			this.groupBox2.Controls.Add(this.OnlineRadioButton);
			this.groupBox2.Location = new System.Drawing.Point(6, 18);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(176, 44);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = " Quickbooks version ";
			// 
			// DesktopRadioButton
			// 
			this.DesktopRadioButton.AutoSize = true;
			this.DesktopRadioButton.Checked = true;
			this.DesktopRadioButton.Location = new System.Drawing.Point(98, 19);
			this.DesktopRadioButton.Name = "DesktopRadioButton";
			this.DesktopRadioButton.Size = new System.Drawing.Size(65, 17);
			this.DesktopRadioButton.TabIndex = 22;
			this.DesktopRadioButton.TabStop = true;
			this.DesktopRadioButton.Text = "Desktop";
			this.DesktopRadioButton.UseVisualStyleBackColor = true;
			this.DesktopRadioButton.Click += new System.EventHandler(this.DesktopRadioButton_Click);
			// 
			// OnlineRadioButton
			// 
			this.OnlineRadioButton.AutoSize = true;
			this.OnlineRadioButton.Location = new System.Drawing.Point(14, 19);
			this.OnlineRadioButton.Name = "OnlineRadioButton";
			this.OnlineRadioButton.Size = new System.Drawing.Size(55, 17);
			this.OnlineRadioButton.TabIndex = 21;
			this.OnlineRadioButton.Text = "Online";
			this.OnlineRadioButton.UseVisualStyleBackColor = true;
			this.OnlineRadioButton.Click += new System.EventHandler(this.OnlineRadioButton_Click);
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(263, 18);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(128, 13);
			this.label14.TabIndex = 6;
			this.label14.Text = "Inventory Code Structure:";
			// 
			// InventoryCodeStructure
			// 
			this.InventoryCodeStructure.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.InventoryCodeStructure.FormattingEnabled = true;
			this.InventoryCodeStructure.Items.AddRange(new object[] {
            "Service Type - Package Type",
            "Package Type - Service Type",
            "Driver Name"});
			this.InventoryCodeStructure.Location = new System.Drawing.Point(237, 34);
			this.InventoryCodeStructure.Name = "InventoryCodeStructure";
			this.InventoryCodeStructure.Size = new System.Drawing.Size(272, 21);
			this.InventoryCodeStructure.TabIndex = 1;
			// 
			// button1
			// 
			this.button1.Image = global::IdsQuickBooks.Properties.Resources.FolderOpen_24x24_72;
			this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button1.Location = new System.Drawing.Point(409, 483);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(100, 23);
			this.button1.TabIndex = 6;
			this.button1.Text = "Browse";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// ErrorFolder
			// 
			this.ErrorFolder.Location = new System.Drawing.Point(21, 483);
			this.ErrorFolder.Name = "ErrorFolder";
			this.ErrorFolder.Size = new System.Drawing.Size(305, 20);
			this.ErrorFolder.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(18, 468);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 13);
			this.label3.TabIndex = 11;
			this.label3.Text = "&Error Folder:";
			// 
			// button3
			// 
			this.button3.Image = global::IdsQuickBooks.Properties.Resources.FolderOpen_24x24_72;
			this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button3.Location = new System.Drawing.Point(409, 434);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(100, 23);
			this.button3.TabIndex = 4;
			this.button3.Text = "Browse";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// IDSExportPath
			// 
			this.IDSExportPath.Location = new System.Drawing.Point(21, 436);
			this.IDSExportPath.Name = "IDSExportPath";
			this.IDSExportPath.Size = new System.Drawing.Size(305, 20);
			this.IDSExportPath.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(18, 421);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(93, 13);
			this.label2.TabIndex = 8;
			this.label2.Text = "&IDS Export Folder:";
			// 
			// OptionsTabControl
			// 
			this.OptionsTabControl.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
			this.OptionsTabControl.Controls.Add(this.OnlineTabPage);
			this.OptionsTabControl.Controls.Add(this.DesktopTabPage);
			this.OptionsTabControl.Location = new System.Drawing.Point(6, 85);
			this.OptionsTabControl.Name = "OptionsTabControl";
			this.OptionsTabControl.SelectedIndex = 0;
			this.OptionsTabControl.Size = new System.Drawing.Size(492, 319);
			this.OptionsTabControl.TabIndex = 2;
			// 
			// OnlineTabPage
			// 
			this.OnlineTabPage.Controls.Add(this.toolStrip1);
			this.OnlineTabPage.Controls.Add(this.label4);
			this.OnlineTabPage.Controls.Add(this.CompanyId);
			this.OnlineTabPage.Controls.Add(this.label5);
			this.OnlineTabPage.Controls.Add(this.AccessTokenKey);
			this.OnlineTabPage.Controls.Add(this.label6);
			this.OnlineTabPage.Controls.Add(this.AccessTokenSecret);
			this.OnlineTabPage.Location = new System.Drawing.Point(4, 25);
			this.OnlineTabPage.Name = "OnlineTabPage";
			this.OnlineTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.OnlineTabPage.Size = new System.Drawing.Size(484, 290);
			this.OnlineTabPage.TabIndex = 0;
			this.OnlineTabPage.Text = "Online";
			this.OnlineTabPage.UseVisualStyleBackColor = true;
			// 
			// toolStrip1
			// 
			this.toolStrip1.Location = new System.Drawing.Point(3, 3);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(478, 25);
			this.toolStrip1.TabIndex = 25;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(10, 9);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(127, 13);
			this.label4.TabIndex = 19;
			this.label4.Text = "QuickBooks &Company Id:";
			// 
			// CompanyId
			// 
			this.CompanyId.Location = new System.Drawing.Point(10, 25);
			this.CompanyId.Name = "CompanyId";
			this.CompanyId.Size = new System.Drawing.Size(233, 20);
			this.CompanyId.TabIndex = 20;
			this.CompanyId.Leave += new System.EventHandler(this.CompanyId_Leave);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(7, 57);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(100, 13);
			this.label5.TabIndex = 21;
			this.label5.Text = "&Access Token Key:";
			// 
			// AccessTokenKey
			// 
			this.AccessTokenKey.Location = new System.Drawing.Point(10, 73);
			this.AccessTokenKey.Name = "AccessTokenKey";
			this.AccessTokenKey.Size = new System.Drawing.Size(384, 20);
			this.AccessTokenKey.TabIndex = 22;
			this.AccessTokenKey.Enter += new System.EventHandler(this.AccessTokenKey_Enter);
			this.AccessTokenKey.Leave += new System.EventHandler(this.AccessTokenKey_Leave);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(7, 109);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(113, 13);
			this.label6.TabIndex = 23;
			this.label6.Text = "&Access Token Secret:";
			// 
			// AccessTokenSecret
			// 
			this.AccessTokenSecret.Location = new System.Drawing.Point(10, 125);
			this.AccessTokenSecret.Name = "AccessTokenSecret";
			this.AccessTokenSecret.Size = new System.Drawing.Size(384, 20);
			this.AccessTokenSecret.TabIndex = 24;
			this.AccessTokenSecret.Enter += new System.EventHandler(this.AccessTokenSecret_Enter);
			this.AccessTokenSecret.Leave += new System.EventHandler(this.AccessTokenSecret_Leave);
			// 
			// DesktopTabPage
			// 
			this.DesktopTabPage.Controls.Add(this.DefaultInvoiceLayout);
			this.DesktopTabPage.Controls.Add(this.label15);
			this.DesktopTabPage.Controls.Add(this.FuelSurchargeCode);
			this.DesktopTabPage.Controls.Add(this.label13);
			this.DesktopTabPage.Controls.Add(this.FuelSurchargeLedger);
			this.DesktopTabPage.Controls.Add(this.label12);
			this.DesktopTabPage.Controls.Add(this.DiscountItemCode);
			this.DesktopTabPage.Controls.Add(this.label11);
			this.DesktopTabPage.Controls.Add(this.CostOfGoodsLedger);
			this.DesktopTabPage.Controls.Add(this.label10);
			this.DesktopTabPage.Controls.Add(this.AssetLedger);
			this.DesktopTabPage.Controls.Add(this.label9);
			this.DesktopTabPage.Controls.Add(this.SalesLedger);
			this.DesktopTabPage.Controls.Add(this.label8);
			this.DesktopTabPage.Controls.Add(this.button2);
			this.DesktopTabPage.Controls.Add(this.QuickbooksCompanyFile);
			this.DesktopTabPage.Controls.Add(this.label7);
			this.DesktopTabPage.Location = new System.Drawing.Point(4, 25);
			this.DesktopTabPage.Name = "DesktopTabPage";
			this.DesktopTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.DesktopTabPage.Size = new System.Drawing.Size(484, 290);
			this.DesktopTabPage.TabIndex = 1;
			this.DesktopTabPage.Text = "Desktop";
			this.DesktopTabPage.UseVisualStyleBackColor = true;
			// 
			// DefaultInvoiceLayout
			// 
			this.DefaultInvoiceLayout.Location = new System.Drawing.Point(11, 69);
			this.DefaultInvoiceLayout.Name = "DefaultInvoiceLayout";
			this.DefaultInvoiceLayout.Size = new System.Drawing.Size(458, 20);
			this.DefaultInvoiceLayout.TabIndex = 2;
			this.DefaultInvoiceLayout.Leave += new System.EventHandler(this.DefaultInvoiceLayout_Leave);
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(8, 53);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(117, 13);
			this.label15.TabIndex = 25;
			this.label15.Text = "Defa&ult Invoice Layout:";
			// 
			// FuelSurchargeCode
			// 
			this.FuelSurchargeCode.Location = new System.Drawing.Point(11, 239);
			this.FuelSurchargeCode.Name = "FuelSurchargeCode";
			this.FuelSurchargeCode.Size = new System.Drawing.Size(148, 20);
			this.FuelSurchargeCode.TabIndex = 7;
			this.FuelSurchargeCode.Leave += new System.EventHandler(this.FuelSurchargeCode_Leave);
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(8, 224);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(110, 13);
			this.label13.TabIndex = 23;
			this.label13.Text = "&Fuel Surcharge Code:";
			// 
			// FuelSurchargeLedger
			// 
			this.FuelSurchargeLedger.Location = new System.Drawing.Point(182, 170);
			this.FuelSurchargeLedger.Name = "FuelSurchargeLedger";
			this.FuelSurchargeLedger.Size = new System.Drawing.Size(148, 20);
			this.FuelSurchargeLedger.TabIndex = 6;
			this.FuelSurchargeLedger.Leave += new System.EventHandler(this.FuelSurchargeLedger_Leave);
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(179, 155);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(118, 13);
			this.label12.TabIndex = 21;
			this.label12.Text = "&Fuel Surcharge Ledger:";
			// 
			// DiscountItemCode
			// 
			this.DiscountItemCode.Location = new System.Drawing.Point(321, 239);
			this.DiscountItemCode.Name = "DiscountItemCode";
			this.DiscountItemCode.Size = new System.Drawing.Size(148, 20);
			this.DiscountItemCode.TabIndex = 8;
			this.DiscountItemCode.Text = "Discount";
			this.DiscountItemCode.Leave += new System.EventHandler(this.DiscountItemCode_Leave);
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(318, 224);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(103, 13);
			this.label11.TabIndex = 19;
			this.label11.Text = "&Discount Item Code:";
			// 
			// CostOfGoodsLedger
			// 
			this.CostOfGoodsLedger.Location = new System.Drawing.Point(13, 170);
			this.CostOfGoodsLedger.Name = "CostOfGoodsLedger";
			this.CostOfGoodsLedger.Size = new System.Drawing.Size(148, 20);
			this.CostOfGoodsLedger.TabIndex = 5;
			this.CostOfGoodsLedger.Leave += new System.EventHandler(this.CostOfGoodsLedger_Leave);
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(10, 155);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(110, 13);
			this.label10.TabIndex = 17;
			this.label10.Text = "&Cost Of Good Ledger:";
			// 
			// AssetLedger
			// 
			this.AssetLedger.Location = new System.Drawing.Point(182, 126);
			this.AssetLedger.Name = "AssetLedger";
			this.AssetLedger.Size = new System.Drawing.Size(148, 20);
			this.AssetLedger.TabIndex = 4;
			this.AssetLedger.Leave += new System.EventHandler(this.AssetLedger_Leave);
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(179, 111);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(69, 13);
			this.label9.TabIndex = 15;
			this.label9.Text = "&AssetLedger:";
			// 
			// SalesLedger
			// 
			this.SalesLedger.Location = new System.Drawing.Point(13, 126);
			this.SalesLedger.Name = "SalesLedger";
			this.SalesLedger.Size = new System.Drawing.Size(148, 20);
			this.SalesLedger.TabIndex = 3;
			this.SalesLedger.Leave += new System.EventHandler(this.SalesLedger_Leave);
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(10, 111);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(72, 13);
			this.label8.TabIndex = 13;
			this.label8.Text = "&Sales Ledger:";
			// 
			// button2
			// 
			this.button2.Image = global::IdsQuickBooks.Properties.Resources.FolderOpen_24x24_72;
			this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.button2.Location = new System.Drawing.Point(369, 22);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(100, 23);
			this.button2.TabIndex = 1;
			this.button2.Text = "Browse";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// QuickbooksCompanyFile
			// 
			this.QuickbooksCompanyFile.Location = new System.Drawing.Point(11, 25);
			this.QuickbooksCompanyFile.Name = "QuickbooksCompanyFile";
			this.QuickbooksCompanyFile.Size = new System.Drawing.Size(305, 20);
			this.QuickbooksCompanyFile.TabIndex = 0;
			this.QuickbooksCompanyFile.Leave += new System.EventHandler(this.QuickbooksCompanyFile_Leave);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(8, 10);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(133, 13);
			this.label7.TabIndex = 10;
			this.label7.Text = "&Quickbooks Company File:";
			// 
			// UserName
			// 
			this.UserName.Location = new System.Drawing.Point(612, 97);
			this.UserName.Name = "UserName";
			this.UserName.Size = new System.Drawing.Size(327, 20);
			this.UserName.TabIndex = 3;
			this.UserName.Visible = false;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(609, 81);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(60, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "&UserName:";
			this.label1.Visible = false;
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(623, 160);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(100, 20);
			this.textBox1.TabIndex = 5;
			this.textBox1.Visible = false;
			// 
			// Password
			// 
			this.Password.AutoSize = true;
			this.Password.Location = new System.Drawing.Point(620, 144);
			this.Password.Name = "Password";
			this.Password.Size = new System.Drawing.Size(56, 13);
			this.Password.TabIndex = 4;
			this.Password.Text = "&Password:";
			this.Password.Visible = false;
			// 
			// TrayIcon
			// 
			this.TrayIcon.BalloonTipTitle = "IDS Quickbooks Import";
			this.TrayIcon.ContextMenuStrip = this.IconTrayMenuStrip;
			this.TrayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("TrayIcon.Icon")));
			this.TrayIcon.Text = "IDS Quickbooks Import";
			this.TrayIcon.BalloonTipClosed += new System.EventHandler(this.TrayIcon_BalloonTipClosed);
			this.TrayIcon.Click += new System.EventHandler(this.TrayIcon_Click);
			// 
			// IconTrayMenuStrip
			// 
			this.IconTrayMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showToolStripMenuItem1,
            this.fileToolStripMenuItem2});
			this.IconTrayMenuStrip.Name = "contextMenuStrip1";
			this.IconTrayMenuStrip.Size = new System.Drawing.Size(104, 48);
			// 
			// showToolStripMenuItem1
			// 
			this.showToolStripMenuItem1.Name = "showToolStripMenuItem1";
			this.showToolStripMenuItem1.Size = new System.Drawing.Size(103, 22);
			this.showToolStripMenuItem1.Text = "&Show";
			this.showToolStripMenuItem1.Click += new System.EventHandler(this.showToolStripMenuItem1_Click);
			// 
			// fileToolStripMenuItem2
			// 
			this.fileToolStripMenuItem2.Name = "fileToolStripMenuItem2";
			this.fileToolStripMenuItem2.Size = new System.Drawing.Size(103, 22);
			this.fileToolStripMenuItem2.Text = "&Exit";
			// 
			// PollTimer
			// 
			this.PollTimer.Interval = 15000;
			this.PollTimer.Tick += new System.EventHandler(this.PollTimer_Tick);
			// 
			// FolderBrowserDialog
			// 
			this.FolderBrowserDialog.Description = "Select Path";
			// 
			// HideButton
			// 
			this.HideButton.Image = global::IdsQuickBooks.Properties.Resources.Expand_large;
			this.HideButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.HideButton.Location = new System.Drawing.Point(445, 573);
			this.HideButton.Name = "HideButton";
			this.HideButton.Size = new System.Drawing.Size(75, 23);
			this.HideButton.TabIndex = 2;
			this.HideButton.Text = "&Hide";
			this.HideButton.UseVisualStyleBackColor = true;
			this.HideButton.Click += new System.EventHandler(this.HideButton_Click);
			// 
			// fileToolStripMenuItem1
			// 
			this.fileToolStripMenuItem1.Name = "fileToolStripMenuItem1";
			this.fileToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
			this.fileToolStripMenuItem1.Text = "&File";
			// 
			// exitToolStripMenuItem1
			// 
			this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
			this.exitToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
			this.exitToolStripMenuItem1.Text = "&Exit";
			this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
			// 
			// showToolStripMenuItem
			// 
			this.showToolStripMenuItem.Name = "showToolStripMenuItem";
			this.showToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.showToolStripMenuItem.Text = "Show";
			this.showToolStripMenuItem.Click += new System.EventHandler(this.showToolStripMenuItem_Click);
			// 
			// Debug
			// 
			this.Debug.AutoSize = true;
			this.Debug.Location = new System.Drawing.Point(11, 579);
			this.Debug.Name = "Debug";
			this.Debug.Size = new System.Drawing.Size(58, 17);
			this.Debug.TabIndex = 3;
			this.Debug.Text = "Debug";
			this.Debug.UseVisualStyleBackColor = true;
			// 
			// ImportBtn
			// 
			this.ImportBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ImportBtn.Location = new System.Drawing.Point(318, 573);
			this.ImportBtn.Name = "ImportBtn";
			this.ImportBtn.Size = new System.Drawing.Size(80, 23);
			this.ImportBtn.TabIndex = 1;
			this.ImportBtn.Text = "&Import";
			this.ImportBtn.UseVisualStyleBackColor = true;
			this.ImportBtn.Click += new System.EventHandler(this.ImportBtn_Click);
			// 
			// MainMenu
			// 
			this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.optionsToolStripMenuItem});
			this.MainMenu.Location = new System.Drawing.Point(0, 0);
			this.MainMenu.Name = "MainMenu";
			this.MainMenu.Size = new System.Drawing.Size(553, 24);
			this.MainMenu.TabIndex = 9;
			this.MainMenu.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "&File";
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
			this.exitToolStripMenuItem.Text = "&Exit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click_1);
			// 
			// optionsToolStripMenuItem
			// 
			this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autoHideOnStartupToolStripMenuItem});
			this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
			this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
			this.optionsToolStripMenuItem.Text = "&Options";
			// 
			// autoHideOnStartupToolStripMenuItem
			// 
			this.autoHideOnStartupToolStripMenuItem.Name = "autoHideOnStartupToolStripMenuItem";
			this.autoHideOnStartupToolStripMenuItem.Size = new System.Drawing.Size(188, 22);
			this.autoHideOnStartupToolStripMenuItem.Text = "&Auto Hide On Startup";
			// 
			// OpenFileDialog
			// 
			this.OpenFileDialog.Filter = "Quickbooks Company File (*.qbw)|*.qbw";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(553, 623);
			this.Controls.Add(this.MainMenu);
			this.Controls.Add(this.ImportBtn);
			this.Controls.Add(this.Debug);
			this.Controls.Add(this.HideButton);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.Password);
			this.Controls.Add(this.UserName);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.groupBox1);
			this.Name = "MainForm";
			this.Text = "IDS QuickBooks V 1.17";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.Resize += new System.EventHandler(this.MainForm_Resize);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.OptionsTabControl.ResumeLayout(false);
			this.OnlineTabPage.ResumeLayout(false);
			this.OnlineTabPage.PerformLayout();
			this.DesktopTabPage.ResumeLayout(false);
			this.DesktopTabPage.PerformLayout();
			this.IconTrayMenuStrip.ResumeLayout(false);
			this.MainMenu.ResumeLayout(false);
			this.MainMenu.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label Password;
		private System.Windows.Forms.TextBox UserName;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NotifyIcon TrayIcon;
		private System.Windows.Forms.Timer PollTimer;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.TextBox IDSExportPath;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.FolderBrowserDialog FolderBrowserDialog;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox ErrorFolder;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button HideButton;
		private System.Windows.Forms.ContextMenuStrip IconTrayMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.ComboBox InventoryCodeStructure;
		private System.Windows.Forms.CheckBox Debug;
		private System.Windows.Forms.ToolStripMenuItem showToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem2;
		private System.Windows.Forms.Button ImportBtn;
		private System.Windows.Forms.MenuStrip MainMenu;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem autoHideOnStartupToolStripMenuItem;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton DesktopRadioButton;
		private System.Windows.Forms.RadioButton OnlineRadioButton;
		private System.Windows.Forms.TextBox AccessTokenSecret;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox AccessTokenKey;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox CompanyId;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TabControl OptionsTabControl;
		private System.Windows.Forms.TabPage OnlineTabPage;
		private System.Windows.Forms.TabPage DesktopTabPage;
		private System.Windows.Forms.TextBox QuickbooksCompanyFile;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.OpenFileDialog OpenFileDialog;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.TextBox CostOfGoodsLedger;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox AssetLedger;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox SalesLedger;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox DiscountItemCode;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox FuelSurchargeLedger;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox FuelSurchargeCode;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox DefaultInvoiceLayout;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.CheckBox IncludeCharges;
	}
}

