﻿
namespace IdsImport
{
	public static class TripLine
	{
		public enum FIELDS
		{
			ACCOUNT_ID = 1,			// B
			INVOICE_NUMBER,			// C
			TRIP_ID,				// D

			SERVICE_LEVEL,			// E
			PACKAGE_TYPE,			// F
			WEIGHT,					// G
			PIECES,					// H
			BASE_RATE,				// I
			TOTAL_LIVE_VALUE,		// J			+ Discount
			DISCOUNT_AMOUNT,		// K
		}

		public static string InventoryKey( Import.INVENTORY_CODE_STRUCTURE type, Row csvRow )
		{
			var PackageType = csvRow[ (int)FIELDS.PACKAGE_TYPE ].AsString.Trim();
			var ServiceLevel = csvRow[ (int)FIELDS.SERVICE_LEVEL ].AsString.Trim();

			switch( type )
			{
			case Import.INVENTORY_CODE_STRUCTURE.BY_PACKAGE_SERVICE:
				return( PackageType + " - " + ServiceLevel );
			default:
				return( ServiceLevel + " - " + PackageType );
			}
		}

	}
}
