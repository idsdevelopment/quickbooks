﻿using System.IO;

namespace IDS_InvoiceExport
{
	internal static class Utils
	{
		internal static string AddPathSeparator( string FullPath )
		{
			return FullPath.TrimEnd().TrimEnd( Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar )
				   + Path.DirectorySeparatorChar;
		}

		public static bool IsEqualOrNull( this string str1, string str2 )
		{
			return string.IsNullOrEmpty( str1 ) || string.IsNullOrEmpty( str2 ) || str1 == str2;
		}
	}
}