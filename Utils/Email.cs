﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace IdsQuickBooks.Utils
{
	public class Email
	{
		private static string DomainMapper( Match match )
		{
			// IdnMapping class with default property values.
			var idn = new IdnMapping();

			var domainName = match.Groups[ 2 ].Value;
			try
			{
				domainName = idn.GetAscii( domainName );
			}
			catch( ArgumentException )
			{
				return null;
			}
			return match.Groups[ 1 ].Value + domainName;
		}


		public static bool IsValidAddress( string emailAddress )
		{
			if( string.IsNullOrEmpty( emailAddress ) )
				return false;

			// Use IdnMapping class to convert Unicode domain names.
			try
			{
				emailAddress = Regex.Replace( emailAddress, @"(@)(.+)$", DomainMapper, RegexOptions.None );
			}
			catch
			{
				return false;
			}

			// Return true if emailAddress is in valid e-mail format.
			try
			{
				return Regex.IsMatch( emailAddress,
									  @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
									  @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
									  RegexOptions.IgnoreCase );
			}
			catch
			{
				return false;
			}
		}
	}
}