﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using IDS_InvoiceExport;

namespace IdsImport
{
	public class ImportAbort : Exception
	{
	}

	public abstract class Import : Disposable
	{
		internal const string ERROR_EXTENSION = ".error.log",
							  CSV_EXTENSION = ".csv";

		public enum INVENTORY_CODE_STRUCTURE
		{
			BY_SERVICE_PACKAGE,
			BY_PACKAGE_SERVICE
		}

		public enum LINE_TYPE
		{
			ACCOUNT = 1,
			TRIP,
			CHARGE_LINE,
			END_OF_INVOICE
		}

		public static INVENTORY_CODE_STRUCTURE _InventoryCodeStructure;

		private readonly ErrorList Errors = new ErrorList();

		private static INVENTORY_CODE_STRUCTURE InventoryCodeStructure
		{
			get { return _InventoryCodeStructure; }
		}

		protected abstract void OnException( Row csvRow, Exception exception );

		protected abstract void BeginUpdate( string errorPath, bool debug );
		protected abstract void EndUpdate( bool errors );

		protected abstract void BeginInvoice( string accountNumber, string invoiceNumber, DateTime invoiceDate, string taxCode,
											  bool exempt, Row accountLineRow );

		protected abstract void EndInvoice( bool errorThrown );

		protected abstract void AccountLine( Row csvRow );
		protected abstract void TripLine( Row csvRow );
		protected abstract void ChargeLine( Row csvRow, double amount );
		protected abstract void TaxLine( Row csvRow, double amount );
		protected abstract void EndOfInvoiceLine( Row csvRow, bool errorThrown );

		internal class ErrorEntry
		{
			internal string Error;
			internal int LineNumber;
		}

		internal class ErrorList : List<ErrorEntry>
		{
			internal void Add( string error, int lineNumber )
			{
				base.Add( new ErrorEntry { Error = error, LineNumber = lineNumber } );
			}

			internal void Add( string invoiceNumber, string error, int lineNumber )
			{
				Add( "(Invoice: " + invoiceNumber + ")  " + error, lineNumber );
			}

			public override string ToString()
			{
				var Retval = new StringBuilder();

				foreach( var Entry in this )
					Retval.Append( "Line Number: " + Entry.LineNumber + ",  Error: " + Entry.Error + "\r\n" );

				return Retval.ToString();
			}

			internal void WriteToStream( Stream stream )
			{
				using( var Writer = new StreamWriter( stream, Encoding.ASCII ) )
				{
					Writer.Write( ToString() );
					Writer.Flush();
					Writer.Close();
				}
			}
		}

		public string TripInventoryKey( Row csvRow )
		{
			return IdsImport.TripLine.InventoryKey( InventoryCodeStructure, csvRow );
		}

		public string ChargeInventoryKey( Row csvRow )
		{
			return IdsImport.ChargeLine.InventoryKey( csvRow );
		}

		public bool IsTax( string text )
		{
			text = text.ToUpper();
			return text.StartsWith( "GST" )
				   || text.StartsWith( "HST" )
				   || text.StartsWith( "PST" )
				   || text.StartsWith( "TAX" )
				   || text.StartsWith( "NON" )
				   || text.StartsWith( "VAT" )
				   || text.StartsWith( "EXEMPT" );
		}

		public bool IsTaxLine( Row csvRow )
		{
			return IsTax( csvRow[ (int) IdsImport.ChargeLine.FIELDS.DESCRIPTION ].AsString.Trim() );
		}


		public bool Execute( string userName, string password, INVENTORY_CODE_STRUCTURE invCodeStructure, string importFile,
							 string errorPath, bool debug )
		{
			errorPath = Utils.AddPathSeparator( errorPath );

			_InventoryCodeStructure = invCodeStructure;

			bool Result;
			Csv ImportCsv = null;

			Errors.Clear();

			var CurrentLineNumber = 0;

			try
			{
				ImportCsv = Csv.ReadFromFile( importFile );

				var TotalRows = ImportCsv.Rows;
				var CurrentInvoiceNumber = "";
				var CurrentAccountNumber = "";
				var ErrorThrown = false;

				try
				{
					BeginUpdate( errorPath, debug );

					for( ; CurrentLineNumber < TotalRows; )
					{
						Row Row = null;

						try
						{
							Row = ImportCsv[ CurrentLineNumber++ ];
							var LineType = int.Parse( Row[ 0 ] );

							switch( (LINE_TYPE) LineType )
							{
							case LINE_TYPE.ACCOUNT:
								string AccountNumber = Row[ (int) IdsImport.AccountLine.FIELDS.ACCOUNT_ID ];
								string InvoiceNumber = Row[ (int) IdsImport.AccountLine.FIELDS.INVOICE_NUMBER ];

								DateTime InvDate;
								if( !DateTime.TryParse( Row[ (int) IdsImport.AccountLine.FIELDS.INVOICE_DATE_CREATED ], out InvDate ) )
									InvDate = DateTime.Now;

								if( CurrentInvoiceNumber != InvoiceNumber || CurrentAccountNumber != AccountNumber )
								{
									if( CurrentInvoiceNumber != "" )
										EndInvoice( ErrorThrown );

									var TaxCode = "";
									var Exempt = false;

									// Scan Forward For Default Tax Type
									for( var I = CurrentLineNumber; I < TotalRows; )
									{
										var R = ImportCsv[ I++ ];
										LineType = int.Parse( R[ 0 ] );

										if( (LINE_TYPE) LineType == LINE_TYPE.CHARGE_LINE && IsTaxLine( R ) )
										{
											TaxCode = R[ (int) IdsImport.ChargeLine.FIELDS.DESCRIPTION ].AsString.Trim();
											// ReSharper disable once CompareOfFloatsByEqualityOperator
											Exempt = R[ (int) IdsImport.ChargeLine.FIELDS.VALUE ].AsDouble == 0;
											break;
										}
									}
									ErrorThrown = false;
									BeginInvoice( CurrentAccountNumber = AccountNumber, CurrentInvoiceNumber = InvoiceNumber, InvDate, TaxCode,
												  Exempt, Row );
								}

								AccountLine( Row );
								break;

							case LINE_TYPE.CHARGE_LINE:
								if( !ErrorThrown )
								{
									var Amount = Row[ (int) IdsImport.ChargeLine.FIELDS.VALUE ].AsDouble;
									// ReSharper disable once CompareOfFloatsByEqualityOperator
									if( Amount != 0 )
									{
										if( IsTaxLine( Row ) )
											TaxLine( Row, Amount );
										else
											ChargeLine( Row, Amount );
									}
								}
								break;

							case LINE_TYPE.END_OF_INVOICE:
								EndOfInvoiceLine( Row, ErrorThrown );
								break;

							case LINE_TYPE.TRIP:
								if( !ErrorThrown )
									TripLine( Row );
								break;

							default:
								Errors.Add( CurrentInvoiceNumber, "Unknown line type: " + LineType, CurrentLineNumber );
								break;
							}
						}
						catch( ImportAbort )
						{
							throw;
						}
						catch( Exception E )
						{
							ErrorThrown = true;
							OnException( Row, E );
							Errors.Add( CurrentInvoiceNumber, E.Message, CurrentLineNumber );
						}
					}

					if( CurrentInvoiceNumber != "" )
						EndInvoice( ErrorThrown );
				}
				catch( Exception E )
				{
					Errors.Add( CurrentInvoiceNumber, E.Message, CurrentLineNumber );
				}
				finally
				{
					EndUpdate( Errors.Count > 0 );
				}
			}
			catch( Exception E )
			{
				Errors.Add( E.Message, CurrentLineNumber );
			}
			finally
			{
				Result = Errors.Count <= 0;

				if( !Result )
				{
					var Id = DateTime.Now.ToString( "yyyyMMddHHmmss" );

					using( var Stream = new FileStream( errorPath + Id + ERROR_EXTENSION, FileMode.Create, FileAccess.ReadWrite ) )
					{
						Errors.WriteToStream( Stream );
					}

					if( ImportCsv != null )
					{
						using( var Stream = new FileStream( errorPath + Id + CSV_EXTENSION, FileMode.Create, FileAccess.ReadWrite ) )
						{
							Csv.Write( ImportCsv, Stream );
						}
					}
					MessageBox.Show( "The last import has errors.\r\nPlease check the error folder for more information.",
									 "Inport Errors", MessageBoxButtons.OK );
				}
				File.Delete( importFile );
			}
			return Result;
		}
	}
}